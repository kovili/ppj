
public class ToArrayAction extends Action{

	public ToArrayAction(int childIndex) {
		super(childIndex);
	}

	@Override
	void executeAction(TreeNode parent) {
		parent.getSymbol().setType(SemanticUtils.toArray(parent.getChild(getChildIndex()).getSymbol().getType()));
	}

}
