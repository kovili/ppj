import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class ScopeNode {
	
	/**Return value of scope.*/
	private VarType returnVal;
	/**List of definitions of variables (e.g. a  = 5;)*/
	List<NodeElement> identifierDefinitions = new ArrayList<>();;
	/**List of variable declarations (e.g. int a;)*/
	List<NodeElement> identifierDeclarations = new ArrayList<>();;
	/**List of definitions of functions. */
	List<NodeElement> functionDefinitions = new ArrayList<>();;
	/**List of function declarations.*/
	List<NodeElement> functionDeclarations = new ArrayList<>();;
	/**This ScopeNode parent node.*/
	ScopeNode parent;
	/**Children nodes.*/
	List<ScopeNode> children;
	/**The slozenaNaredba which contains this element */
	NodeElement containerSlozenaNaredba;
	
	/**Constructor for when the return value is known. If not known at the moment, use setReturnVal to set.*/
	public ScopeNode( ScopeNode parent, VarType returnVal,  NodeElement containerFunction) {
		this.containerSlozenaNaredba = containerFunction;
		this.returnVal = returnVal;
		this.parent = parent;
		if(parent != null) {
			parent.addScope(this);
		}
	}
	
	/**Constructor for when the return value is known. If not known at the moment, use setReturnVal to set.*/
	public ScopeNode(ScopeNode parent, VarType returnVal) {
		this(parent, returnVal, null);
	}
	
	/**Constructor for when only the parent node is known.*/
	public ScopeNode(ScopeNode parent) {
		this(parent, null, null);
	}
	
	/**Adds a new scope - usually this means a new function body was defined.*/
	public boolean addScope(ScopeNode scope) {
		if(children == null) {
			children = new ArrayList<>();
		}
		return children.add(scope);
	}
	
	//a = 5;
	public boolean addIdentifierDefinition(NodeElement definition) {
		if(identfierDeclarationExists(definition)) {
			return identifierDefinitions.add(definition);
		}
		return false;
	}
	
	//int a;
	public boolean addIdentifierDeclaration(NodeElement declaration) {
		if(declaration.getType() == VarType.CHAR || declaration.getType() == VarType.INT) {
			declaration.setLEQ(LExpressionQualifier.YES);
		} else {
			declaration.setLEQ(LExpressionQualifier.NO);
		}
		if(!identifierDeclarations.contains(declaration)) {
			return identifierDeclarations.add(declaration);
		}
		return false;
	}
	
	public boolean checkAllFunctionsDeclared() {
		boolean flag = true;
		if(children != null) {
			for(int i = 0; i < children.size(); i++) {
				if(!children.get(i).checkAllFunctionsDeclared()) {
					flag = false;
				}
			}
		}
		for(NodeElement function : functionDeclarations) {
			if(findFunctionDefinitionByName(function.getName()) == null) {
				flag = false;
			}
		}
		return flag;

	}
	
	
	public boolean addFunctionDefinition(NodeElement definition) {
		if(findFunctionDefinitionByName(definition.getName()) != null) {
			return false;
		}
		
		if(functionDeclarationExists(definition)) {
			return functionDefinitions.add(definition);
		}
		return false;
	}
	
	public boolean addFunctionDeclaration(NodeElement declaration) {

		NodeElement otherIdentifier = findIdentifierDeclarationByNameLocally(declaration.getName());
		if(otherIdentifier != null) {
			if(!declaration.equals(otherIdentifier)) {
				return false;
			}
		}
		if(!functionDeclarations.contains(declaration) ) {
			return functionDeclarations.add(declaration);
		}
		return true;
	}

	private NodeElement findIdentifierDeclarationByNameLocally(String name) {
		for(NodeElement identifier : identifierDeclarations) {
			if(identifier.getNameParameter().equals(name)) {
				return identifier;
			}
		}
		return null;
	}

	public boolean identfierDeclarationExists(NodeElement definition) {
		if(identifierDeclarations.contains(definition) || (parent != null && parent.identfierDeclarationExists(definition))) {
			return true;
		} else if(findFunctionDeclarationByName(definition.getNameParameter()) != null) {
			return true;
		} else {
			return false;
		}
	}
	
	public boolean functionDeclarationExists(NodeElement definition) {
		if(functionDeclarations.contains(definition) || (parent != null && parent.functionDeclarationExists(definition))) {
			return true;
		} else {
			return false;
		}
	}
	
	public boolean functionDefinitionExists(NodeElement definition) {
		if(functionDefinitions.contains(definition) || (parent != null && parent.functionDeclarationExists(definition))) {
			return true;
		} else {
			return false;
		}
	}
	
	public NodeElement findFunctionDeclarationByName(String name) {
		for(NodeElement function : functionDeclarations) {
			if(function.getName().equals(name)) {
				return function;
			}
		}
		if(parent == null) {
			return null;
		} else {
			return parent.findFunctionDeclarationByName(name);
		}
	}
	
	public NodeElement findFunctionDeclarationByNameLocally(String name) {
		for(NodeElement function : functionDeclarations) {
			if(function.getName().equals(name)) {
				return function;
			}
		}
		return null;
	}
	
	public NodeElement findFunctionDefinitionByName(String name) {
		for(NodeElement function : functionDefinitions) {
			if(function.getName().equals(name)) {
				return function;
			}
		}
		return null;
	}
	
	public NodeElement findIdentifierDeclarationByName(String name) {
		for(NodeElement identifier : identifierDeclarations) {
			if(identifier.getNameParameter().equals(name)) {
				return identifier;
			}
		}
		if(parent == null) {
			return null;
		} else {
			return parent.findIdentifierDeclarationByName(name);
		}
	}
	
	public VarType getReturnVal() {
		return returnVal;
	}

	public void setReturnVal(VarType returnVal) {
		this.returnVal = returnVal;
	}

	public NodeElement getContainerFunction() {
		return containerSlozenaNaredba;
	}

	public void setContainerFunction(NodeElement containerFunction) {
		this.containerSlozenaNaredba = containerFunction;
	}

	public ScopeNode getParent() {
		return parent;
	}

	public void setParent(ScopeNode parent) {
		this.parent = parent;
	}

	public List<NodeElement> getDefinitions() {
		return identifierDefinitions;
	}

	public List<NodeElement> getDeclarations() {
		return identifierDeclarations;
	}

	public List<ScopeNode> getChildren() {
		return children;
	}

	@Override
	public String toString() {
		if(parent == null) {
			return "GLOBAL NODE";
		} else {
			return "SomeFunc";
		}
	}
	
	
	

}
