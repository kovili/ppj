
public class AddNameAction extends Action {

	public AddNameAction(int childIndex) {
		super(childIndex);
	}

	@Override
	void executeAction(TreeNode parent) {
		if(!parent.getSymbol().addName(parent.getChild(getChildIndex()).getSymbol().getNameParameter())) {
			parent.getChild(getChildIndex()).getSymbol().signalError();
		}
		
	}

}
