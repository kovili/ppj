
public class GetChildNameAction extends Action {

	public GetChildNameAction(int childIndex) {
		super(childIndex);
	}

	@Override
	void executeAction(TreeNode parent) {
		parent.getSymbol().setNameParameter(parent.getChild(getChildIndex()).getSymbol().getNameParameter());
		
	}

}
