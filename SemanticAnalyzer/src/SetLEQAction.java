
public class SetLEQAction extends Action {
	
	private LExpressionQualifier qualifier;

	public SetLEQAction(int childIndex, LExpressionQualifier qualifier) {
		super(childIndex);
		this.qualifier = qualifier;
	}

	@Override
	void executeAction(TreeNode parent) {
		parent.getSymbol().setLEQ(qualifier);
	}

}
