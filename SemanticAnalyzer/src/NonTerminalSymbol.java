/**
 * Represents a NTSymbol.
 * These are all symbols in the generative syntax tree surrounded by '<' and '>'.
 * @author Petar
 *
 */
public class NonTerminalSymbol extends NodeElement {
	
	public NonTerminalSymbol(String name) {
		super(name);
	}
	
	public String getNormalizedName() {
		return getName().substring(1, getName().length() - 1);
	}
	
	@Override
	void printError() {
		System.out.print(" " +  getName());
		
	}


	@Override
	public String toString() {
		return getName();
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((getName() == null) ? 0 : getName().hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		NonTerminalSymbol other = (NonTerminalSymbol) obj;
		if (getName() == null) {
			if (other.getName() != null)
				return false;
		} else if (!getName().equals(other.getName()))
			return false;
		return true;
	}

	@Override
	void checkParameter() {
		return;
	}

	@Override
	void check() {
		return;
	}

	@Override
	String getNameParameter() {
		// TODO Auto-generated method stub
		return null;
	}
	
}
