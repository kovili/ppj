import java.util.ArrayList; 
import java.util.List;

/**
 * A single tree node in the syntax tree.
 * @author Petar
 */
public class TreeNode {
	/**Name of the terminal/non-terminal sign symbol.*/
	private NodeElement symbol;
	/**Parent node.*/
	private TreeNode parent;
	/**Direct children nodes of this node.*/
	private List<TreeNode> children = new ArrayList<>();
	/**Defines the actions for this particular node.*/
	private List<Action> semanticActions;
	/**The scope that this node can "see".*/
	private ScopeNode scope;
	
	public TreeNode(NodeElement symbol) {
		this.symbol = symbol;
		symbol.setContainer(this);
	}
	
	public TreeNode(NodeElement symbol, ScopeNode scope) {
		this(symbol);
		this.scope = scope;
	}

	public TreeNode getChild(int index) {
		return children.get(index);
	}
	
	public NodeElement getSymbol() {
		return symbol;
	}

	public void setSymbol(NodeElement symbol) {
		this.symbol = symbol;
	}

	/**
	 * Adds a child to the {@link #children} list. 
	 * Additionally, sets the parent of child node to this node.
	 * Additionally additionally, sets the child scope0.
	 */
	public boolean addChild(TreeNode child) {
		if(children == null) {
			children = new ArrayList<>();
		}
		child.setParent(this);
		if(!child.getSymbol().getName().equals("<slozena_naredba>")) {
			child.setScope(this.scope);
		} else {
			ScopeNode newScope = new ScopeNode(this.scope);
			child.setScope(newScope);
			newScope.setContainerFunction(this.getSymbol());
		}
		return children.add(child);
	}


	/**
	 * Gets the parent n levels above the current node.
	 * @param level - Equals n
	 * @return Parent node n levels above
	 */
	public TreeNode getParent(int level) {
		if(level == 0) {
			return this;
		} else if(level > 0){
			return parent.getParent(--level);
		}
		throw new IllegalArgumentException("Level must be non-negative.");
	}
	
	/**
	 * Prints tree from given node, added for debugging purposes.
	 * @param depth - Determines the number of trailing whitespaces.
	 */
	public void printTree(int depth) {
		StringBuilder whitespaceBuilder = new StringBuilder();
		for(int i = 0; i < depth; i++) whitespaceBuilder.append(" ");
		whitespaceBuilder.append(this.toString());
		System.out.println(whitespaceBuilder.toString());
		if(children != null) {
			children.forEach((child) -> child.printTree(depth + 1));
		}
	}
	
	public void printError() {
		System.out.print(symbol.getName() + " ::=");
		for(TreeNode child : children) {
			child.getSymbol().printError();
		}
		System.out.println();
	}
	
	public void analyze() {
		semanticActions = SemanticUtils.getActions(symbol.getName(), children);
		initializeIdentifierNames();
		semanticActions.forEach((action) -> action.executeAction(this));
	}
	
	private void initializeIdentifierNames() {
		if(children != null) {
			children.forEach((child) -> child.getSymbol().initializeName());
		}
	}
	
	public void setParent(TreeNode parent) {
		this.parent = parent;
	}
	
	
	public TreeNode getParent() {
		return parent;
	}
	
	public String getName() {
		return symbol.getName();
	}

	public void setName(NodeElement symbol) {
		this.symbol = symbol;
	}
	
	
	public ScopeNode getScope() {
		return scope;
	}

	public void setScope(ScopeNode scope) {
		this.scope = scope;
	}
	

	@Override
	public String toString() {
		return symbol.getName();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((symbol == null) ? 0 : symbol.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TreeNode other = (TreeNode) obj;
		if (symbol == null) {
			if (other.symbol != null)
				return false;
		} else if (!symbol.equals(other.symbol))
			return false;
		return true;
	}

	public int searchForArraySize() {
		for(TreeNode child : children) {
			int childArraySize = child.getSymbol().stringSizeRequest();
			if(childArraySize != 0) {
				return childArraySize;
			}
		}
		return 0;
	}

	public int stringSizeRequest() {
		for(TreeNode child : children) {
			int childArraySize = child.getSymbol().stringSizeRequest();
			if(childArraySize != 0) {
				return childArraySize;
			}
		}
		return 0;
	}

	

	
	
	
	
	
}
