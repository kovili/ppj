/**
 * Action which forces chosen child (which for this action is usually a lex unit) to check the validity of it's value.
 * @author Petar
 *
 */
public class CheckParamaterAction extends Action {

	public CheckParamaterAction(int childIndex) {
		super(childIndex);
	}

	@Override
	public void executeAction(TreeNode parent) {
		parent.getChild(getChildIndex()).getSymbol().checkParameter();
	}
	
}
