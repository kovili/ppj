
public class CheckFunctionTypeAction extends Action {

	
	private String parameters;

	public CheckFunctionTypeAction(int childIndex, String parameters) {
		super(childIndex);
		this.parameters = parameters;
	}

	@Override
	void executeAction(TreeNode parent) {
		String functioName = parent.getChild(getChildIndex()).getSymbol().getValue();
		NodeElement node = parent.getScope().findFunctionDeclarationByName(functioName);
		Function function = null;
		
		if(node != null) {
			function = (Function) node;
			if(parameters.equals("PARAMS")) {
				for(VarType type : function.getArgs()) {
					if(type == VarType.VOID) {
						parent.getChild(getChildIndex()).getSymbol().signalError();
					}
				}
			} else if(parameters.equals("VOID")){
				if(!(function.getArgs().size() == 1 && function.getArgs().get(0) == VarType.VOID)) {
					parent.getChild(getChildIndex()).getSymbol().signalError();
				}
			} else {
				throw new IllegalArgumentException("Invalid argument for function action CHECKFD.");
			}
		} else {
			parent.getChild(getChildIndex()).getSymbol().signalError();
		}
	}
	
	

}
