
public class CreateFunctionAction extends Action {

	public CreateFunctionAction(int childIndex) {
		super(childIndex);
	}

	@Override
	void executeAction(TreeNode parent) {
		IDN functionName = (IDN) parent.getChild(1).getSymbol(); //identifikator na 2. mjestu u produkciji
		NodeElement type =  parent.getChild(0).getSymbol();//tip na 1. mjestu u produkciji
		DefinicijaFunkcije definition = (DefinicijaFunkcije) parent.getSymbol();//mozda redundantno
		Function function = null;
		
		
		if(parent.getChild(3).getSymbol() instanceof ListaParametara) {
			ListaParametara listaParametara = (ListaParametara) parent.getChild(3).getSymbol();//lista parametara na 4. mjestu u produkciji koja je sadri
			function = new Function(functionName, type.getType(), listaParametara.getArgs());
			definition.setFunction(function);
			parent.setSymbol(definition);
		}
		
		else if(parent.getChild(3).getSymbol() instanceof LexicalUnit) {//KR_VOID na 4. mjestu u produkciji
			function = new Function(functionName, type.getType());
			definition.setFunction(function);
			parent.setSymbol(definition);
		}
		if(!parent.getScope().addFunctionDeclaration(function) || !parent.getScope().addFunctionDefinition(function)) {
			parent.getChild(0).getSymbol().signalError();
		}
	}

}
