
public class DefinicijaFunkcije extends NonTerminalSymbol {
	
	private Function function;

	public DefinicijaFunkcije(String name) {
		super(name);
	}

	public Function getFunction() {
		return function;
	}

	public void setFunction(Function function) {
		this.function = function;
	}

}
