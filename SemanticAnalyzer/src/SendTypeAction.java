
public class SendTypeAction extends Action {

	public SendTypeAction(int childIndex) {
		super(childIndex);
	}

	@Override
	public void executeAction(TreeNode parent) {
		parent.getSymbol().setType(parent.getChild(getChildIndex()).getSymbol().getType());
	}

}
