
public class FunctionReturnTypeRequestAction extends Action {

	public FunctionReturnTypeRequestAction(int childIndex) {
		super(childIndex);
	}

	@Override
	void executeAction(TreeNode parent) {
		NodeElement element = parent.getScope().findFunctionDeclarationByName(parent.getChild(getChildIndex()).getSymbol().getValue());
		Function function = null;
		if(element != null) function = (Function) element;
		parent.getSymbol().setType(function.getReturnType());
	}

}
