import java.util.ArrayList;
import java.util.List;

public class ListaArgumenata extends NonTerminalSymbol{
	
	private List<VarType> args;

	public ListaArgumenata(String name) {
		super(name);
	}

	@Override
	public void addArg(VarType type) {
		if(args == null) {
			args = new ArrayList<>();
		}
		args.add(type);
	}

	@Override
	public List<VarType> getArgs() {
		return args;
	}

	@Override
	public void setArgs(List<VarType> args) {
		this.args = args;
	}
	
	
	

}
