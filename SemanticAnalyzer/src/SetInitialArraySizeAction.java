import java.util.ArrayList;
import java.util.List;

public class SetInitialArraySizeAction extends Action {
	
	private String option;

	public SetInitialArraySizeAction(int childIndex, String option) {
		super(childIndex);
		this.option = option;
	}

	@Override
	void executeAction(TreeNode parent) {
		if(option.equals("BROJ")) {
			parent.getSymbol().setArraySize(((BROJ) parent.getChild(2).getSymbol()).getNumValue());
		} else if(option.equals("ONE")) {
			parent.getSymbol().setArraySize(1);
			parent.getSymbol().setType(parent.getSymbol().getArgs().get(0));
		} else if(option.equals("NIZZNAKOVA")) {
			int arraySize = parent.stringSizeRequest();
			if(arraySize != 0) {
				parent.getSymbol().setArraySize(arraySize);
				List<VarType> args = new ArrayList<>();
				for(int i = 0; i < arraySize; i++) args.add(VarType.CHAR);
				parent.getSymbol().setArgs(args);
				parent.getSymbol().setType(VarType.CONST_CHAR_ARRAY);
			} else {
				parent.getSymbol().setType(parent.getChild(0).getSymbol().getType());
			}
		} else if(option.equals("PLUSONE")) {
			parent.getSymbol().setArraySize(parent.getChild(getChildIndex()).getSymbol().getArraySize() + 1);
			parent.getSymbol().setType(parent.getSymbol().getArgs().get(0));
		} else if(option.equals("NORMAL")) {
			parent.getSymbol().setArraySize(parent.getChild(getChildIndex()).getSymbol().getArraySize());
		}
	}

}
