
public class ZNAK extends LexicalUnit {
	
	private int ASCIIValue;

	public ZNAK(String fullName, String []lexInfo) {
		super(fullName, lexInfo);
		setType(VarType.INT);
		setLEQ(LExpressionQualifier.NO);
	}
	
	public ZNAK(String fullName) {
		super(fullName);
	}

	public void checkParameter() {
		this.ASCIIValue = (int) SemanticUtils.getCharacter(getValue().substring(1, getValue().length() - 1));
	}

}
