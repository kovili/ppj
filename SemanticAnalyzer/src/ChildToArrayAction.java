
public class ChildToArrayAction extends Action {

	public ChildToArrayAction(int childIndex) {
		super(childIndex);
	}

	@Override
	void executeAction(TreeNode parent) {
		parent.getChild(getChildIndex()).getSymbol().setType(SemanticUtils.toArray(parent.getSymbol().getInheritedType()));
		
	}

}
