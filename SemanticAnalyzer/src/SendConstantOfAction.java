
public class SendConstantOfAction extends Action {

	public SendConstantOfAction(int childIndex) {
		super(childIndex);
	}

	@Override
	void executeAction(TreeNode parent) {
		parent.getSymbol().setType(SemanticUtils.toConst(parent.getChild(getChildIndex()).getSymbol().getType()));
	}
	
	

}
