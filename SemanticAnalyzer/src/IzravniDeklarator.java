
public class IzravniDeklarator extends NonTerminalSymbol {
	
	private VarType inheritedType;

	public IzravniDeklarator(String name) {
		super(name);
	}

	@Override
	public VarType getInheritedType() {
		return inheritedType;
	}

	@Override
	public void setInheritedType(VarType type) {
		this.inheritedType = type;
	}

	
}
