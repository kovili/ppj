/**
 * Interface for variables which may be constants. To be seen if this interface is needed.
 * @author Petar
 *
 */
public interface ConstQualified {
	void setConstFlag();
	boolean isConst();
}
