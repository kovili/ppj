import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList; 
import java.util.List;
import java.util.Map;

import javax.management.RuntimeErrorException;

import java.util.HashMap;

/**
 * Helper class which contains methods commonly used in other parts of the program.
 * <br>
 * Additionally, this class contains the most important helper structure in the program - The semantic production dictionary.
 * 
 * @author Petar
 *
 */
public class SemanticUtils {
	/**For each production NTSymbol -> 	produkcija
	 * this dictionary defines a list of actions to be executed. 
	 * This is the main procedure with which the L-attribute translation grammar is realized.
	 */
	private static HashMap<String, HashMap<List<TreeNode>, List<Action>>> semanticProductionDictionary = new HashMap<>();
	/**String path to program language syntax file.*/
	private static String SYNTAX_RULE_FILE = Paths.get("").toAbsolutePath().toString() + "/src/syntax.txt";
	/**String path to program language syntax file.*/
	private static String ACTION_FILE = Paths.get("").toAbsolutePath().toString() + "/src/actions.txt";
	/**To be returned for productions which don't require any action.*/
	private static List<Action> emptyAction = new ArrayList<>();
	
	private static final char []ALLOWED_ESCAPING = {'t', 'n', '0', '\\', '\'', '"'};
	 
	public static final String CONST_CHAR_END = "\\0";
	
	public static HashMap<String, VarType []> varTypes = new HashMap<>();
	
	public static Function MAIN = new Function(new IDN("IDN 0 main", new String[]{"IDN", "0", "main"}), VarType.INT);
	
	private static final HashMap<String, LExpressionQualifier> qualifiers = new HashMap<>();
	
	//Static block which reads from the syntax and action files and creates the semanticProductionDictionary.
	static {
		try {
			BufferedReader syntaxReader = new BufferedReader(new InputStreamReader(new FileInputStream(SYNTAX_RULE_FILE)));
			BufferedReader actionReader = new BufferedReader(new InputStreamReader(new FileInputStream(ACTION_FILE)));
			int lineNumber  = 0;
			emptyAction.add(new NoAction());
			addVarTypes();
			addQualifiers();
			MAIN.initializeName();
			//First three line of syntax declaration %V %T %SYN
			syntaxReader.readLine();
			syntaxReader.readLine();
			syntaxReader.readLine();
			String currentProduction = null;
			while(syntaxReader.ready()) {
				String line = syntaxReader.readLine();
				List<TreeNode> currentElements;
				List<Action> currentActions;
				
				//EJ OVO JE NOVI NEZAVRSNI ZNAK SA SVOJIM PRODUKCIJAMA, AJ GA STAVI U RJECNIK
				if(!line.startsWith(" ")) {
					actionReader.readLine();
					lineNumber++;
					currentProduction = line;
					semanticProductionDictionary.put(currentProduction, new HashMap<>());
				//EJ OVO JE JEDNA OD PRODUKCIJA NEZAVRSNOG ZNAKA KOJEG SAM UPRAVO STAVIO U RJECNIK
				//O DA ISTO TAKO MOLIM TE STAVI I AKCIJE ZA OVU PRODUKCIJU
				} else {
					lineNumber++;
					currentElements = new ArrayList<>();
					currentActions = new ArrayList<>();
					
					String []productionSymbols = line.trim().split(" ");
					for(String symbol : productionSymbols) {
						currentElements.add(new TreeNode(findEmptySymbol(symbol)));
					}
					
					String []actionSymbols = actionReader.readLine().trim().split(" ");
					if(actionSymbols.length != 0) {
						for(String action : actionSymbols) {
							currentActions.add(findAction(action, lineNumber, line));
						}
					} else {
						System.err.println("BLANK LINE - NO ACTION");
					}
					
					semanticProductionDictionary.get(currentProduction).put(currentElements, currentActions);
				}
			}
			syntaxReader.close();
			actionReader.close();
		} catch (IOException e) {
			System.err.println(e.getMessage());
			throw new RuntimeException("Nesto je poslo krivo s izgradnjom rjecnika :(");
		}
		//SLJEDECE DVIJE LINIJE NEPOTREBNE, ODKOMENTIRATI SAMO AKO SE TESTIRA RJECNIK
		//HashMap<String, HashMap<List<TreeNode>, List<Action>>> test = semanticProductionDictionary;
		//System.err.println("TEST JE UPALJEN U SEMANTICUTILS");
		
		List<VarType> mainArgs = new ArrayList<>();
		mainArgs.add(VarType.VOID);
		MAIN.setArgs(mainArgs);

	}
	
	/**Get actions for one production.*/
	public static List<Action> getActions(String productionName, List<TreeNode> symbols) { 
		HashMap<List<TreeNode>, List<Action>> resultMap = semanticProductionDictionary.get(productionName);
		if(resultMap != null) {
			return resultMap.get(symbols);
		}
		return emptyAction;
		
	}
	
	private static void addQualifiers() {
		qualifiers.put("YES", LExpressionQualifier.YES);
		qualifiers.put("NO", LExpressionQualifier.NO);
	}

	private static void addVarTypes() {
		
		VarType []voidField = { VarType.VOID };
		varTypes.put("VOID", voidField);
		VarType []intField = { VarType.INT };
		varTypes.put("INT", intField);
		VarType []charField = { VarType.CHAR };
		varTypes.put("CHAR", charField);
		VarType []constIntArrayField = { VarType.CONST_INT_ARRAY };
		varTypes.put("CONST_INT_ARRAY", constIntArrayField);
		VarType []constCharArrayField = { VarType.CONST_CHAR_ARRAY };
		varTypes.put("CONST_CHAR_ARRAY",  constCharArrayField);
		VarType []constIntField = { VarType.CONST_INT };
		varTypes.put("CONST_INT",  constIntField);
		VarType []constCharField = { VarType.CONST_CHAR };
		varTypes.put("CONST_CHAR",  constCharField);
		VarType []intArrayField = { VarType.INT_ARRAY };
		varTypes.put("INT_ARRAY", intArrayField);
		VarType []charArrayField = { VarType.CHAR_ARRAY };
		varTypes.put("CHAR_ARRAY", charArrayField);
		VarType []constTField = { VarType.CONST_CHAR, VarType.CONST_INT };
		varTypes.put("CONST_T",  constTField);
		VarType []functionField = { VarType.FUNCTION };
		varTypes.put("FUNCTION", functionField);
		varTypes.put("INTLIKE", new VarType[]{ VarType.INT, VarType.CHAR, VarType.CONST_INT, VarType.CONST_CHAR });
		varTypes.put("CONSTLIKE", new VarType[]{ VarType.CONST_INT, VarType.CONST_CHAR, VarType.CONST_CHAR_ARRAY, VarType.CONST_INT_ARRAY});
		varTypes.put("INTONLY", new VarType[]{ VarType.INT, VarType.CONST_INT});
		varTypes.put("CHARONLY", new VarType[]{ VarType.CHAR, VarType.CONST_CHAR});
		varTypes.put("INTARRAYONLY", new VarType[]{ VarType.INT_ARRAY, VarType.CONST_INT_ARRAY});
		varTypes.put("CHARARRAYONLY", new VarType[]{ VarType.CHAR_ARRAY, VarType.CONST_CHAR_ARRAY});
		varTypes.put("ARRAYONLY", new VarType[]{ VarType.INT_ARRAY, VarType.CONST_INT_ARRAY, VarType.CHAR_ARRAY, VarType.CONST_CHAR_ARRAY});
		varTypes.put("NOTCONST(T)", new VarType[]{ VarType.INT, VarType.CHAR, VarType.INT_ARRAY, VarType.CONST_INT_ARRAY, VarType.CHAR_ARRAY, VarType.CONST_CHAR_ARRAY});
	}

	public static boolean validEscaping(char []characters) {
		boolean escaping = false;
		for(char character : characters) {
			if(escaping) {
				boolean validEscape = false;
				for(char allowedChar : ALLOWED_ESCAPING) {
					if(character == allowedChar) {
						escaping = false;
						validEscape = true;
						break;
					}
				}
				if(!validEscape) {
					return false;
				}
			} else {
				if(character == '\\') {
					escaping = true;
				}
			}
		}
		return true;
	}
		
	
	
	/**
	 * Creates production from the text of the actions.txt file.<br>
	 * Each action is fully described by the action name and childIndex.<br>
	 * Format of each action description in the actions.txt folder is ACTION_NAME-CHILD_INDEX.<br>
	 * <ul>
	 * 		<li><code>SENDT is the name for the {@link SendTypeAction}</code> </li>
	 * 		<li><code>SENDL is the name for the {@link SendLExpressionQualifierAction}</code></li>
	 * 		<li><code>CHECKP is the name for the {@link CheckParamaterAction}</code></li>
	 * 		<li><code>One space and a newline defines the {@link NoAction}</code></li>
	 * </ul>
	 * 
	 */
	private static Action findAction(String lineContent, int lineNumber, String wholeLine) {
		String []parts = lineContent.split("-");
		if(parts.length > 1) {
			String actionName = parts[0];
			int childIndex = Integer.parseInt(parts[1]);
			if(actionName.equals("SENDT")) {
				return new SendTypeAction(childIndex);
			} else if(actionName.equals("SENDL")) {
				return new SendLExpressionQualifierAction(childIndex);
			} else if(actionName.equals("CHECKP")) {
				return new CheckParamaterAction(childIndex);
			} else if(actionName.equals("CHECKD")) {
				return new CheckIfDeclaredAction(childIndex);
			} else if(actionName.equals("CREATEF")) {
				return new CreateFunctionAction(childIndex);
			} else if(actionName.equals("CHECKRT")) {
				return new CheckRequiredTypesAction(childIndex, varTypes.get(parts[2]));
			} else if(actionName.equals("CHECKFT")) {
				return new CheckForbiddenTypesAction(childIndex, varTypes.get(parts[2]));
			} else if(actionName.equals("SENDCO")) {
				return new SendConstantOfAction(childIndex);
			} else if(actionName.equals("SETL")) {
				return new SetLEQAction(childIndex, qualifiers.get(parts[2]));
			} else if(actionName.equals("SETT")) {
				return new SetTypeAction(childIndex, varTypes.get(parts[2])[0]);
			} else if(actionName.equals("RETREQ")) {
				return new FunctionReturnTypeRequestAction(childIndex);
			} else if(actionName.equals("ADDARG")) {
				return new AddArgAction(childIndex);
			} else if(actionName.equals("SETARGS")) {
				return new SetArgsAction(childIndex);
			} else if(actionName.equals("CHECKC")) {
				return new CheckCompatibilityAction(childIndex, Integer.parseInt(parts[2]));
			} else if(actionName.equals("CHECKL")) {
				return new CheckLExpressionQualifierAction(childIndex, qualifiers.get(parts[2]));
			} else if(actionName.equals("SETLIF")) {
				return new SetLExpressionQualifierIfAction(childIndex, varTypes.get(parts[2]));
			} else if(actionName.equals("CHECKFUNCT")) {
				return new CheckFunctionTypeAction(childIndex, parts[2]);
			} else if(actionName.equals("EXTRACTFROMARRAY")) {
				return new ExtractTypeFromArrayAction(childIndex);
			} else if(actionName.equals("CHECKARGS")) {
				return new CheckArgsAction(childIndex, Integer.parseInt(parts[2]));
			} else if(actionName.equals("CHECKCAST")) {
				return new CheckExplicitCastAction(childIndex, Integer.parseInt(parts[2]));
			} else if(actionName.equals("CONFIRMINLOOP")) {
				return new ConfirmInsideLoopAction(childIndex);
			} else if(actionName.equals("CONFIRMINFUNC")) {
				return new ConfirmInFunctionAction(childIndex, parts[2]);
			} else if(actionName.equals("SETNAMES")) {
				return new SetNamesAction(childIndex);
			} else if(actionName.equals("ADDNAME")) {
				return new AddNameAction(childIndex);
			} else if(actionName.equals("GETNAME")) {
				return new GetChildNameAction(childIndex);
			} else if(actionName.equals("TOARRAY")) {
				return new ToArrayAction(childIndex);
			} else if(actionName.equals("AZ")) {
				return new AnalyzeAction(childIndex);
			} else if(actionName.equals("TRANSFERP")) {
				return new TransferParametersAction(childIndex, Integer.parseInt(parts[2]));
			} else if(actionName.equals("INHERITTYPE")) {
				if(parts.length == 3) {
					return new InheritTypeAction(childIndex, Integer.parseInt(parts[2]));
				} else {
					return new InheritTypeAction(childIndex);
				}
			} else if(actionName.equals("MEGA")) {
				return new MegaAction(childIndex);
			} else if(actionName.equals("DECLAREID")) {
				return new DeclareIdentifierAction(childIndex);
			} else if(actionName.equals("INHERITARRAY")) {
				return new InheritArrayAction(childIndex);
			} else if(actionName.equals("CHECKARRAYSIZE")) {
				return new CheckArraySizeAction(childIndex);
			} else if(actionName.equals("SETINITARRSIZE")) {
				return new SetInitialArraySizeAction(childIndex, parts[2]);
			} else if(actionName.equals("DECLAREFUNC")) {
				return new DeclareFunctionAction(childIndex, parts[2]);
			} else if(actionName.equals("CHILDTOARRAY")) {
				return new ChildToArrayAction(childIndex);
			} else if(actionName.equals("ADDLISTEXP")) {
				return new AddListExpression(childIndex);
			} else if(actionName.equals("SETINITTYPE")) {
				return new SetInitTypeAction(childIndex);
			}
		}
		//System.err.println("The name of the action is wrong! Line in action folder is: " + lineNumber);
		return new NoAction();
	}
	
	/**Determines if the next NodeElement is a NTSymbol or LexUnit*/
	public static NodeElement findSymbol(String lineContent) {
		if(lineContent.startsWith("<")) {
			return findNonTerminalSymbol(lineContent);
		} else {
			return findLexicalUnit(lineContent);
		}
	}
	
	/**Determines if the next NodeElement is a NTSymbol or LexUnit*/
	public static NodeElement findEmptySymbol(String lineContent) {
		if(lineContent.startsWith("<")) {
			return findNonTerminalSymbol(lineContent);
		} else {
			return findEmptyLexicalUnit(lineContent);
		}
	}
	
	/**Creates a dummy lex unit.*/
	private static NodeElement findEmptyLexicalUnit(String lineContent) {
		if(lineContent.startsWith("BROJ")) {
			return new BROJ(lineContent);
		} else if(lineContent.startsWith("IDN")) {
			return new IDN(lineContent);
		} else if(lineContent.startsWith("NIZ_ZNAKOVA")) {
			return new NIZ_ZNAKOVA(lineContent);
		} else {
			return new LexicalUnit(lineContent);
		}
	}
	
	/**
	 * Creates a specific lex unit if it is one of the known lex units (e.g. BROJ), otherwise creates a general lex unit.
	 */
	private static NodeElement findLexicalUnit(String lineContent) {
		if(lineContent.startsWith("BROJ")) {
			return new BROJ(lineContent, lineContent.split(" "));
		} else if(lineContent.startsWith("IDN")) {
			return new IDN(lineContent, lineContent.split(" "));
		} else if(lineContent.startsWith("NIZ_ZNAKOVA")) {
			return new NIZ_ZNAKOVA(lineContent, lineContent.split(" "));
		} else {
			return new LexicalUnit(lineContent, lineContent.split(" "));
		}
	}
	
	/**
	 * Creates a specific NTSymbol if it is one of the known NTSymbols (e.g. prijevodna_jedinica), otherwise creates a general NTSymbol.
	 */
	private static NodeElement findNonTerminalSymbol(String lineContent) {
		if(lineContent.equals("<lista_argumenata>")) {
			return new ListaArgumenata(lineContent);
		}  else if(lineContent.equals("<slozena_naredba>")) {
			return new SlozenaNaredba(lineContent);
		} else if(lineContent.equals("<definicija_funkcije>")) {
			return new DefinicijaFunkcije(lineContent);
		} else if(lineContent.equals("<lista_parametara>")) {
			return new ListaParametara(lineContent);
		} else if(lineContent.equals("<deklaracija_parametra>")) {
			return new DeklaracijaParametara(lineContent);
		}  else if(lineContent.equals("<izravni_deklarator>")) {
			return new IzravniDeklarator(lineContent);
		} 
		else {
			return new NonTerminalSymbol(lineContent);
		}
	}
	
	public static VarType toConst(VarType type) {
		if(type == VarType.INT) {
			return VarType.CONST_INT;
		} else if(type == VarType.CHAR) {
			return VarType.CONST_CHAR;
		} else if(type == VarType.INT_ARRAY) {
			return VarType.INT_ARRAY;
		} else if(type == VarType.CHAR_ARRAY) {
			return VarType.CONST_CHAR_ARRAY;
		} else {
			return type;
		}
		
	}
	
	public static boolean checkCastCompatibility(VarType left, VarType right) {
		VarType []intLike = varTypes.get("INTLIKE");
		
		for(VarType rightCandidate : intLike) {
			if(right == rightCandidate) {
				for(VarType leftCandidate : intLike) {
					if(left == leftCandidate)  {
						return true;
					}
				}
			}
		}
		return false;
	}
	
	public static boolean checkImplicitCompatibility(VarType left, VarType right) {
		if(left == right) return true;
		if(checkIntLikeCompatibility(left, right)) {
			return true;
		} else {
			return checkArrayOnlyCompatibility(left, right);
		}
	}

	//TODO POPRAVI OVO LMAO
	private static boolean checkArrayOnlyCompatibility(VarType left, VarType right) {
		if(right == VarType.CONST_CHAR_ARRAY) {
			return left == VarType.CONST_CHAR_ARRAY || left == VarType.CHAR_ARRAY || left == VarType.CONST_INT_ARRAY || left == VarType.INT_ARRAY;
		} else if(right == VarType.CONST_INT_ARRAY) {
			return left == VarType.CONST_INT_ARRAY || left == VarType.INT_ARRAY;
		}
		return false;
	}

	private static boolean checkIntLikeCompatibility(VarType left, VarType right) {
		VarType []intLike = varTypes.get("INTLIKE");
		VarType []intOnly = varTypes.get("INTONLY");
		VarType []charOnly = varTypes.get("CHARONLY");
		
		for(VarType candidate : charOnly) {
			if(left == candidate) {
				for(VarType rightCandidate : charOnly) {
					if(right == rightCandidate) {
						return true;
					}
				}
			}
		}
		
		for(VarType candidate : intOnly) {
			if(left == candidate) {
				for(VarType rightCandidate : intLike) {
					if(right == rightCandidate) {
						return true;
					}
				}
			}
		}
		return false;
		
	}
	

	private static boolean checkIfInt(VarType right) {
		return (right == VarType.INT || right == VarType.CONST_INT);
	}

	private static boolean checkIfChar(VarType right) {
		return (right == VarType.CHAR || right == VarType.CONST_CHAR);
	}

	public static char getCharacter(String character) {
		if(character.length() == 1) {
			return character.charAt(0);
		} else if(character.length() == 2) {
			char secondChar = character.charAt(1);
			if(secondChar == 't') return '\t';
			if(secondChar == 'n') return '\n';
			if(secondChar == '0') return '\0';
			if(secondChar == '\\') return '\\';
			if(secondChar == '\'') return '\'';
			if(secondChar == '"') return '"';
		}
		throw new IllegalArgumentException("Invalid String to character conversion");
	}
	
	public static VarType extractTypeFromArray(VarType arrayType) {
		if(arrayType == VarType.CHAR_ARRAY) {
			return VarType.CHAR;
		} else if(arrayType == VarType.INT_ARRAY) {
			return VarType.INT;
		} else if(arrayType == VarType.CONST_INT_ARRAY) {
			return VarType.CONST_INT;
		} else if(arrayType == VarType.CONST_CHAR_ARRAY) {
			return VarType.CONST_CHAR;
		}
		throw new IllegalArgumentException("Can't convert from non-array.");
	}

	public static VarType toArray(VarType type) {
		if(type == VarType.CHAR) {
			return VarType.CHAR_ARRAY;
		} else if(type == VarType.INT) {
			return VarType.INT_ARRAY;
		} else if(type == VarType.CONST_CHAR) {
			return VarType.CONST_CHAR_ARRAY;
		} else if(type == VarType.CONST_INT) {
			return VarType.CONST_INT_ARRAY;
		}
		return type;
	}
	
	public static VarType removeCons(VarType type) {
		if(type == VarType.CONST_CHAR) {
			return VarType.CHAR;
		} else if(type == VarType.CONST_INT) {
			return VarType.INT;
		} if(type == VarType.CONST_CHAR_ARRAY) {
			return VarType.CHAR_ARRAY;
		} else if(type == VarType.CONST_INT_ARRAY) {
			return VarType.INT_ARRAY;
		}
		return type;
	}
}
