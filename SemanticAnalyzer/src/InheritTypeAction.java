
public class InheritTypeAction extends Action {
	
	private int inheritorIndex;

	public InheritTypeAction(int childIndex, int inheritorIndex) {
		super(childIndex);
		this.inheritorIndex = inheritorIndex;
	}

	public InheritTypeAction(int childIndex) {
		this(childIndex, -1);
	}

	@Override
	void executeAction(TreeNode parent) {
		if(inheritorIndex == -1) {
			parent.getChild(getChildIndex()).getSymbol().setInheritedType(parent.getSymbol().getInheritedType());
		} else {
			parent.getChild(inheritorIndex).getSymbol().setInheritedType(parent.getChild(getChildIndex()).getSymbol().getInheritedType());
		}
	}

}
