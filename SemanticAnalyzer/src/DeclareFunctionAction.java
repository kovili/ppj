import java.util.ArrayList;
import java.util.List;

public class DeclareFunctionAction extends Action {
	
	String option;

	public DeclareFunctionAction(int childIndex, String option) {
		super(childIndex);
		this.option = option;
	}

	@Override
	void executeAction(TreeNode parent) {
		VarType returnType = parent.getSymbol().getInheritedType();
		IDN identifier = (IDN) parent.getChild(0).getSymbol();
		Function function = null;
		if(option.equals("VOID")) {
			function = new Function(identifier, returnType);
		} else if(option.equals("PARAMS")) {
			function = new Function(identifier, returnType, parent.getChild(2).getSymbol().getArgs());
		}
		if(!parent.getScope().addFunctionDeclaration(function)) {
			parent.getChild(0).getSymbol().signalError();
		}
		
	}

}
