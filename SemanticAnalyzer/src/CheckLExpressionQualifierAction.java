
public class CheckLExpressionQualifierAction extends Action {
	
	LExpressionQualifier requiredQualifier;

	public CheckLExpressionQualifierAction(int childIndex, LExpressionQualifier requiredQualifier) {
		super(childIndex);
		this.requiredQualifier = requiredQualifier;
	}

	@Override
	void executeAction(TreeNode parent) {
		parent.getChild(getChildIndex()).getSymbol().checkLEQ(requiredQualifier);
	}

}
