/**Represent a single end-node of the syntax tree.*/
public class LexicalUnit extends NodeElement{
	
	/**Name of the lexical unit.*/
	private String lexUnit;
	
	/**Lex unit line in program.*/
	private String lexUnitLine;
	
	/**Value of lexical unit.*/
	private String value;
	
	/**Real constructor to be used when a real representation of the lex unit is needed.*/
	public LexicalUnit(String fullName, String []lexInfo) {
		super(fullName);
		this.lexUnit = lexInfo[0];
		this.lexUnitLine = lexInfo[1];
		this.value = lexInfo[2];
	}
	
	/**'Dummy' constructor used for the global dictionary.*/
	public LexicalUnit(String lexUnit) {
		super(lexUnit);
		this.lexUnit = lexUnit;
	}
	
	
	public void printError() {
		System.out.print(" " + this.lexUnit.toString() + "(" + lexUnitLine + "," + value + ")");
	}

	@Override
	public String toString() {
		return getName();
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getLexUnit() {
		return lexUnit;
	}

	public String getLexUnitLine() {
		return lexUnitLine;
	}

	@Override
	public int getArraySize() {
		return 0;
	}

	public String getNormalizedName() {
		return value.substring(1, value.length() - 1);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((lexUnit == null) ? 0 : lexUnit.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		LexicalUnit other = (LexicalUnit) obj;
		if (lexUnit == null) {
			if (other.lexUnit != null)
				return false;
		} else if (!lexUnit.equals(other.lexUnit))
			return false;
		return true;
	}

	@Override
	void checkParameter() {
		return;
	}

	@Override
	void check() {
		return;
	}

	@Override
	String getNameParameter() {
		return getValue();
	}
	
}
