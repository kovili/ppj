
public class SetInitTypeAction extends Action {

	public SetInitTypeAction(int childIndex) {
		super(childIndex);
	}

	@Override
	void executeAction(TreeNode parent) {
		parent.getSymbol().setType(SemanticUtils.toArray(SemanticUtils.toConst(parent.getChild(1).getSymbol().getType())));
	}

}
