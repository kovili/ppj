
public class ConfirmInsideLoopAction extends Action {

	public ConfirmInsideLoopAction(int childIndex) {
		super(childIndex);
	}
	
	public ConfirmInsideLoopAction() {
		super(0);
	}

	@Override
	void executeAction(TreeNode parent) {
		TreeNode currentParent = parent.getParent();
		while(currentParent != null) {
			if(currentParent.getSymbol().getName().equals("<naredba_petlje>")) {
				return;
			}
			currentParent = currentParent.getParent();
		}
		parent.getChild(0).getSymbol().signalError();
	}

}
