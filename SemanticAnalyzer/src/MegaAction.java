import java.util.List;

public class MegaAction extends Action {

	public MegaAction(int childIndex) {
		super(childIndex);
	}

	@Override
	void executeAction(TreeNode parent) {
		VarType type = parent.getChild(0).getSymbol().getType();
		VarType []intLikes = SemanticUtils.varTypes.get("INTLIKE");
		VarType []arrayOnly = SemanticUtils.varTypes.get("ARRAYONLY");
		for(VarType intLike : intLikes) {
			if(SemanticUtils.checkImplicitCompatibility(type, parent.getChild(2).getSymbol().getType())) {
				
				parent.getChild(2).getSymbol().setInheritedType(SemanticUtils.removeCons(type));
				return;
			} else {
				parent.getChild(0).getSymbol().signalError();
			}
		}
		for(VarType arrayType : arrayOnly) {
			if(type == arrayType) {
				NodeElement deklarator = parent.getChild(0).getSymbol();
				NodeElement init = parent.getChild(2).getSymbol();
				
				if(init.getArraySize() <= deklarator.getArraySize()) {
					List<VarType> initArgs = init.getArgs();
					List<VarType> deklaratorArgs = deklarator.getArgs();
					
					for(int i = 0; i < deklarator.getArraySize(); i++) {
						if(!SemanticUtils.checkImplicitCompatibility(deklaratorArgs.get(i), initArgs.get(i))) {
							parent.getChild(0).getSymbol().signalError();
						}
					}
					return;
				}
			}
		}
		parent.getChild(0).getSymbol().signalError();
	}

}
