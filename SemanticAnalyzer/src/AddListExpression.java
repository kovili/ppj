
public class AddListExpression extends Action {

	public AddListExpression(int childIndex) {
		super(childIndex);
	}

	@Override
	void executeAction(TreeNode parent) {
		parent.getSymbol().addArg(parent.getChild(getChildIndex()).getSymbol().getType());
		parent.getSymbol().setType(SemanticUtils.toConst(parent.getSymbol().getArgs().get(0)));
		
	}

}
