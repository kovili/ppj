
public class CheckIfDeclaredAction extends Action {

	public CheckIfDeclaredAction(int childIndex) {
		super(childIndex);
	}

	@Override
	void executeAction(TreeNode parent) {
		parent.getChild(getChildIndex()).getSymbol().checkIdentifierDeclaration(parent.getScope());
	}
	
}
