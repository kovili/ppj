
public class IDN extends LexicalUnit {
	
	private String variableName;

	public IDN(String lexUnit) {
		super(lexUnit);
	}
	
	public IDN(String lexUnit, String[] lexInfo) {
		super(lexUnit, lexInfo);
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((getLexUnit() == null) ? 0 : getLexUnit().hashCode());
		result = prime * result + ((variableName == null) ? 0 : variableName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		IDN other = (IDN) obj;
		if (getLexUnit() == null) {
			if (other.getLexUnit() != null)
				return false;
		} else if (!getLexUnit().equals(other.getLexUnit()))
			return false;
		if (variableName == null) {
			if (other.getVariableName() != null)
				return false;
		} else if (!variableName.equals(other.getVariableName()))
			return false;
		return true;
	}

	public String getVariableName() {
		return variableName;
	}

	public void initializeName() {
		this.variableName = getNameParameter();
	}

}
