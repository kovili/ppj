import java.util.List;

public class CheckArgsAction extends Action {
	
	private int argsListIndex;

	public CheckArgsAction(int childIndex, int argsListIndex) {
		super(childIndex);
		this.argsListIndex = argsListIndex;
	}

	@Override
	void executeAction(TreeNode parent) {
		NodeElement node = parent.getScope().findFunctionDeclarationByName(parent.getChild(getChildIndex()).getSymbol().getValue());
		Function function = null;
		if(node != null) {
			function = (Function) node;
			List<VarType> funcArgs = function.getArgs();
			List<VarType> args = parent.getChild(argsListIndex).getSymbol().getArgs();
			if(args.size() != funcArgs.size()) {
				parent.getChild(getChildIndex()).getSymbol().signalError();
			}
			for(int i = 0; i < args.size(); i++) {
				if(!SemanticUtils.checkImplicitCompatibility(funcArgs.get(i), args.get(i))) {
					parent.getChild(getChildIndex()).getSymbol().signalError();
				}
			}
		} else {
			parent.getChild(getChildIndex()).getSymbol().signalError();
		}
	}

}
