
public class CheckCompatibilityAction extends Action {
	
	private int otherChild;

	public CheckCompatibilityAction(int childIndex, int otherChild) {
		super(childIndex);
		this.otherChild = otherChild;
	}

	@Override
	void executeAction(TreeNode parent) {
		parent.getChild(getChildIndex()).getSymbol().checkCompatibility(parent.getChild(otherChild).getSymbol());
	}

}
