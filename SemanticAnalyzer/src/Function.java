import java.util.ArrayList;
import java.util.List;

public class Function extends NodeElement {

	private IDN identifier;
	private VarType returnType;

	private List<VarType> args;
	
	
	public Function(IDN identifier, VarType returnType, List<VarType> args) {
		super(identifier.getValue());
		this.identifier = identifier;
		this.returnType = returnType;
		this.args = args;
	}
	
	public Function(IDN identifier, VarType returnType) {
		super(identifier.getValue());
		this.identifier = identifier;
		this.returnType = returnType;
		this.args = new ArrayList<>();
		args.add(VarType.VOID);
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((args == null) ? 0 : args.hashCode());
		result = prime * result + ((identifier == null) ? 0 : identifier.hashCode());
		result = prime * result + ((returnType == null) ? 0 : returnType.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Function other = (Function) obj;
		if (args == null) {
			if (other.args != null)
				return false;
		} else if (!args.equals(other.args))
			return false;
		if (identifier == null) {
			if (other.identifier != null)
				return false;
		} else if (!identifier.equals(other.identifier))
			return false;
		if (returnType != other.returnType)
			return false;
		return true;
	}


	public VarType getReturnType() {
		return returnType;
	}
	public void setReturnType(VarType returnType) {
		this.returnType = returnType;
	}
	public List<VarType> getArgs() {
		return args;
	}
	public void setArgs(List<VarType> args) {
		this.args = args;
	}

	@Override
	String getNormalizedName() {
		return "func" +  identifier.getValue();
	}

	@Override
	String getName() {
		return identifier.getValue();
	}

	@Override
	void printError() {
	}

	@Override
	void checkParameter() {
	}

	@Override
	void check() {
	}

	@Override
	String getNameParameter() {
		return getValue();
	}
	
	@Override
	public void initializeName() {
		this.identifier.initializeName();
	}
	
	public IDN getIdentifier() {
		return identifier;
	}

	public void setIdentifier(IDN identifier) {
		this.identifier = identifier;
	}
	
}
