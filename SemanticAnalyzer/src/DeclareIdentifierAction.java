
public class DeclareIdentifierAction extends Action {

	public DeclareIdentifierAction(int childIndex) {
		super(childIndex);
	}

	@Override
	void executeAction(TreeNode parent) {
		if(!parent.getScope().addIdentifierDeclaration(parent.getChild(getChildIndex()).getSymbol())) {
			parent.getChild(getChildIndex()).getSymbol().signalError();
		}
		
	}

}
