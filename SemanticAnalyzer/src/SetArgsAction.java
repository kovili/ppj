
public class SetArgsAction extends Action {

	public SetArgsAction(int childIndex) {
		super(childIndex);
	}

	@Override
	void executeAction(TreeNode parent) {
		parent.getSymbol().setArgs(parent.getChild(getChildIndex()).getSymbol().getArgs());	
	}

}
