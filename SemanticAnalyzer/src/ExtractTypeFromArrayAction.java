
public class ExtractTypeFromArrayAction extends Action {

	public ExtractTypeFromArrayAction(int childIndex) {
		super(childIndex);
	}

	@Override
	void executeAction(TreeNode parent) {
		parent.getSymbol().setType(SemanticUtils.extractTypeFromArray(parent.getChild(getChildIndex()).getSymbol().getType()));
	}

}
