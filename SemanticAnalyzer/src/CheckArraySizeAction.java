
public class CheckArraySizeAction extends Action {

	public CheckArraySizeAction(int childIndex) {
		super(childIndex);
	}

	@Override
	void executeAction(TreeNode parent) {
		BROJ arraySize = (BROJ) parent.getChild(2).getSymbol();
		if(arraySize.getNumValue() < 0 || arraySize.getNumValue() > 1024) {
			parent.getChild(2).getSymbol().signalError();
		}
		
	}

}
