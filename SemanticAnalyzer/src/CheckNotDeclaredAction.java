
public class CheckNotDeclaredAction extends Action {

	public CheckNotDeclaredAction(int childIndex) {
		super(childIndex);
	}

	@Override
	void executeAction(TreeNode parent) {
		if(parent.getScope().identfierDeclarationExists(parent.getChild(getChildIndex()).getSymbol())) {
			parent.getChild(0).getSymbol().signalError();
		}
		
	}

}
