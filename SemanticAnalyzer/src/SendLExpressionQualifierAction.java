
public class SendLExpressionQualifierAction extends Action{

	public SendLExpressionQualifierAction(int childIndex) {
		super(childIndex);
	}

	@Override
	public void executeAction(TreeNode parent) {
		parent.getSymbol().setLEQ(parent.getChild(getChildIndex()).getSymbol().getLEQ());
	}

}
