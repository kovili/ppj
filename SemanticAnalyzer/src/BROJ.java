/**
 * Represents the BROJ lexical unit.
 * @author Petar
 *
 */
public class BROJ extends LexicalUnit{
	
	/**Numerical value of lex unit.*/
	private int numValue;

	/**Constructor which is used for 'dummy' lex units. Dummy lex units only know the Lex unit name (e.g. IDN, BROJ, ZNAK)*/
	public BROJ(String fullName) {
		super(fullName);
	}
	
	/**
	 * Constructs model of BROJ lex unit parsed from generative syntax tree.
	 * This constructor has the added parameter lexInfo because it's supposed to be seperate from the 'dummy' constructor.
	 * @param fullName - consists of LEX_UNIT_NAME LINE VALUE
	 * @param lexInfo - Generally the parts of the fullName although it CAN be anything.
	 */
	public BROJ(String fullName, String []lexInfo) {
		super(fullName, lexInfo);
		setType(VarType.INT);
		setLEQ(LExpressionQualifier.NO);
	}

	@Override
	void checkParameter() {
		try {
			this.numValue = Integer.parseInt(getValue());
		} catch (NumberFormatException e) {
			signalError();
		}
	}

	public int getNumValue() {
		return numValue;
	}

	public void setNumValue(int numValue) {
		this.numValue = numValue;
	}
}
