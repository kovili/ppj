
public class SetNamesAction extends Action {

	public SetNamesAction(int childIndex) {
		super(childIndex);
	}

	@Override
	void executeAction(TreeNode parent) {
		parent.getSymbol().setNames(parent.getChild(getChildIndex()).getSymbol().getNames());
		
	}

}
