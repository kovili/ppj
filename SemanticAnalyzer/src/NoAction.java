/**Dummy action. Used for productions which aren't implemented in the actions.txt folder.*/
public class NoAction extends Action{

	public NoAction(int childIndex) {
		super(childIndex);
	}
	
	public NoAction() {
		super(0);
	}

	@Override
	void executeAction(TreeNode parent) {
		return;
	}
	
	

}
