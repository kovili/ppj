import java.util.ArrayList;
import java.util.List;

/**
 * Represents 'something' in a {@link TreeNode}. Ofcourse, this something is almost certainly a {@link LexicalUnit} or a {@link NonTerminalSymbol}.
 * @author Petar
 *
 */
abstract public class NodeElement {

	/**Element name.*/
	private String name;
	/**Element type.*/
	private VarType type;
	/**Is element an L-Expression? Find out.*/
	private LExpressionQualifier LEQ;
	/**The {@link TreeNode} that contains this element.*/
	private TreeNode container;
	
	private List<VarType> arguments = new ArrayList<>();
	
	private int arraySize = 0;
	
	public NodeElement(String name) {
		this.name = name;
	}
	
	public NodeElement(String name, TreeNode container) {
		this(name);
		this.container = container;
	}
	
	/**Returns name as is found in the generating syntax tree file.*/
	String getName() {
		return this.name;
	}
	
	/**Return normalized name.
	 *<br>
	 *For NTSymbols this means they won't have "<" and ">" in their name.
	 *<br>
	 *For Lexical Units this means they will return only the names of the lexical units (e.g. IDN, KR_CHAR, NIZ_ZNAKOVA...) 
	 */
	abstract String getNormalizedName();
	
	/**Prints the element as is described in the PPJ Laboratory Prep.*/
	abstract void printError();
	
	/**Checks the validity of the node value. Only lex units use this.*/
	abstract void checkParameter();
	
	/**Signals the {@link #container} parent to write an error message.*/
	public void signalError() {
		container.getParent().printError();
		System.exit(0);
	}
	
	public boolean inheritScope() {
		return true;
	}
	
	public void checkIdentifierDeclaration(ScopeNode scope) {
		if(!scope.identfierDeclarationExists(this)) {
			signalError();
		}
		NodeElement copy = scope.findIdentifierDeclarationByName(getNameParameter());
		if(scope.findFunctionDeclarationByNameLocally(getNameParameter()) != null) {
			copy = ((Function) scope.findFunctionDeclarationByNameLocally(getNameParameter())).getIdentifier();
		}
		if(copy != null) {
			setType(copy.type);
			setLEQ(copy.LEQ);
		}
	}
	
	public void checkRequiredTypes(VarType ...requiredTypes) {
		boolean flag = true;
		for(VarType requiredType : requiredTypes)
			if(this.type == requiredType) {
				flag = false;
			}
		if(flag) {
			signalError();
		}
	}
	
	abstract String getNameParameter();
	
	public void checkForbiddenTypes(VarType ...forbiddenTypes) {
		boolean flag = false;
		for(VarType forbiddenType : forbiddenTypes)
			if(this.type == forbiddenType) {
				flag = true;
			}
		if(flag) {
			signalError();
		}
	}
	
	public void checkLEQ(LExpressionQualifier requiredQualifier) {
		if(this.LEQ != requiredQualifier) {
			signalError();
		}
	}
	
	public String getValue() {
		return container.getChild(0).getSymbol().getValue();
	}
	
	/**Might be useless? To be seen.*/
	abstract void check();
	
	VarType getType() {
		return this.type;
	}
	
	void setType(VarType type) {
		this.type = type;
	}
	
	LExpressionQualifier getLEQ() {
		return this.LEQ;
	}
	
	void setLEQ(LExpressionQualifier LEQ) {
		this.LEQ = LEQ;
	}
	
	public TreeNode getContainer() {
		return container;
	}
	
	public void setNameParameter(String name) {
		
	}
	
	public int getStringSize() {
		if(arraySize != 0) {
			return arraySize;
		}
		if(this.container != null) {
			return this.container.stringSizeRequest();
		}
		return arraySize;
	}
	
	public int getArraySize() {
		if(arraySize != 0) {
			return arraySize;
		}
		if(this.container != null) {
			return this.container.searchForArraySize();
		}
		return arraySize;
	}
	
	public void setArraySize(int arraySize) {
		this.arraySize = arraySize;
	}

	public void setContainer(TreeNode container) {
		this.container = container;
	}
	
	public void addArg(VarType type) {
		this.arguments.add(type);
	}
	
	public List<VarType> getArgs() {
		return arguments;
	}
	
	public void setNames(List<String> names) {
	}
	
	public boolean addName(String name) {
		return false;
	}
	
	public List<String> getNames() {
		return null;
	}
	
	public void setArgs(List<VarType> types) {
		this.arguments = types;
	}
	
	public void checkCompatibility(NodeElement other) {
		if(!SemanticUtils.checkImplicitCompatibility(this.getType(), other.getType())) {
			signalError();
		}
	}
	
	public VarType getInheritedType() {
		return this.type;
	}
	
	public void setInheritedType(VarType type) {
		this.type = type;
	}
	

	public void initializeName() {
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		NodeElement other = (NodeElement) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	public int stringSizeRequest() {
		if(container != null) {
			return container.stringSizeRequest();
		} else {
			return 0;
		}
	}
	
}
