import java.util.ArrayList;
import java.util.List;

public class ListaParametara extends NonTerminalSymbol {
	
	List<VarType> parameters;
	
	private List<String> names;
	
	public ListaParametara(String name) {
		super(name);
	}
	public List<VarType> getParameters() {
		return parameters;
	}
	public void addParameter(VarType parameter) {
		if(parameters == null) {
			parameters = new ArrayList<>();
		}
		parameters.add(parameter);
	}
	
	@Override
	public void setArgs(List<VarType> types) {
		this.parameters = types;
	} 
	@Override
	public void addArg(VarType type) {
		addParameter(type);
	}
	@Override
	public List<VarType> getArgs() {
		return getParameters();
	}
	@Override
	public List<String> getNames() {
		return names;
	}
	
	@Override
	public boolean addName(String name) {
		if(names == null) {
			names = new ArrayList<>();
		}
		if(names.contains(name)) {
			return false;
		}
		return names.add(name);
	}
	@Override
	public void setNames(List<String> names) {
		this.names = names;
	}
	
	
	

	
}
