
public class ConfirmInFunctionAction extends Action {
	
	
	private String returnType;

	public ConfirmInFunctionAction(int childIndex, String returnType) {
		super(childIndex);
		this.returnType = returnType;
	}
	

	@Override
	void executeAction(TreeNode parent) {
		TreeNode currentElement = parent.getParent();
		while(currentElement != null) {
			if(currentElement.getSymbol().getName().equals("<definicija_funkcije>")) {
				Function function = ((DefinicijaFunkcije) currentElement.getSymbol()).getFunction();
				if(returnType.equals("VOID")) {
					if(function.getReturnType() != VarType.VOID) parent.getChild(0).getSymbol().signalError();
				} else if(returnType.equals("POV")) {
					if(!SemanticUtils.checkImplicitCompatibility(function.getReturnType(), parent.getChild(getChildIndex()).getSymbol().getType())) {
						parent.getChild(0).getSymbol().signalError();
					}
				} else {
					throw new UnsupportedOperationException("This command is not defined for ConfirmFunctionAction");
				}
			}
			currentElement = currentElement.getParent();
		}
	}

}
