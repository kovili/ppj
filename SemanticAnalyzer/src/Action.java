/**
 * Defines an abstract Action between a node and one of it's children.
 * @author Petar
 *
 */
public abstract class Action {
	
	/**Index of the child node in the {@link TreeNode} children list.*/
	private int childIndex;
	
	public Action(int childIndex) {
		this.childIndex = childIndex;
	}
	
	/**Executes an abstract action. Usually this is done in the form of passing or checking parameters.*/
	abstract void executeAction(TreeNode parent);

	public int getChildIndex() {
		return childIndex;
	}

	public void setChildIndex(int childIndex) {
		this.childIndex = childIndex;
	}
}
