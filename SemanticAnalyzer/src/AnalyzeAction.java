
public class AnalyzeAction extends Action {

	public AnalyzeAction(int childIndex) {
		super(childIndex);
	}

	@Override
	void executeAction(TreeNode parent) {
		parent.getChild(getChildIndex()).analyze();
	}

}
