
public class InheritArrayAction extends Action {

	public InheritArrayAction(int childIndex) {
		super(childIndex);

	}

	@Override
	void executeAction(TreeNode parent) {
		parent.getChild(getChildIndex()).getSymbol().setType(SemanticUtils.toArray(parent.getSymbol().getType()));
	}
	

}
