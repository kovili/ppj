
public class SetTypeAction extends Action {
	
	private VarType type;

	public SetTypeAction(int childIndex, VarType type) {
		super(childIndex);
		this.type = type;
	}

	@Override
	void executeAction(TreeNode parent) {
		parent.getSymbol().setType(type);
		
	}

}
