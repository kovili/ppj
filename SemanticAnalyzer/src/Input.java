import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Entry point for semantic analyzer.
 * User should configure input file to generating syntax tree file.
 * @author Petar
 *
 */
public class Input {
	public static void main(String[] args) throws IOException {
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		
		ScopeNode globalScope = new ScopeNode(null);
		TreeNode head = new TreeNode(SemanticUtils.findSymbol(reader.readLine()), globalScope);
		createTree(head, reader, 0);
		
		head.analyze();
		if(!head.getScope().functionDeclarationExists(SemanticUtils.MAIN)) {
			System.out.println("main");
		}
		if(!globalScope.checkAllFunctionsDeclared()) {
			System.out.println("funkcija");
		}
	} 
	


	/**
	 * Counts spaces lmao. 
	 */
	private static int countSpaces(char []text) {
		int counter = 0;
		for(int i = 0; i < text.length; i++) {
			if(!Character.isWhitespace(text[i])) {
				break;
			}
			counter++;
		}
		return counter;
	}
	
	/**
	 * Recursion for creating tree structure.
	 * 
	 * @param parentNode - Current top-level node
	 * @param reader - Generating syntax tree file reader
	 * @param depth - Current top-level node depth in the syntax tree, head node depth is 0
	 */
	private static void createTree(TreeNode parentNode, BufferedReader reader, int depth) throws IOException {
		if(reader.ready()) {
			String name = reader.readLine();
			TreeNode newNode = new TreeNode(SemanticUtils.findSymbol(name.trim()));
			char []lineChars = name.toCharArray();
			int newDepth = countSpaces(lineChars);
			
			if(newDepth - depth == 1) {
				parentNode.addChild(newNode);
				createTree(newNode, reader, newDepth);
			}
			
			if(newDepth - depth < 1) {
				int parentLevel = depth - newDepth + 1;
				TreeNode newParent = parentNode.getParent(parentLevel);
				newParent.addChild(newNode);
				createTree(newNode, reader, newDepth);
				
			}
		}
	}
	

}
