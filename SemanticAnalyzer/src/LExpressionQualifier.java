/**
 * Describes if the expression is an L-Expression.
 * YES = 1
 * NO  = 0
 * @author Petar
 *
 */
public enum LExpressionQualifier {
	YES,
	NO,
}
