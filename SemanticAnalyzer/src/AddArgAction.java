
public class AddArgAction extends Action {

	public AddArgAction(int childIndex) {
		super(childIndex);
	}

	@Override
	void executeAction(TreeNode parent) {
		parent.getSymbol().addArg(parent.getChild(getChildIndex()).getSymbol().getType());
	}

}
