
public class SetLExpressionQualifierIfAction extends Action {
	
	private VarType []booleanTypes;

	public SetLExpressionQualifierIfAction(int childIndex, VarType []booleanTypes) {
		super(childIndex);
		this.booleanTypes = booleanTypes;
	}

	@Override
	void executeAction(TreeNode parent) {
		LExpressionQualifier parentLEQ = LExpressionQualifier.NO;
		VarType childType = parent.getChild(0).getSymbol().getType();
		for(VarType type : booleanTypes) {
			if(childType == type) {
				parentLEQ = LExpressionQualifier.YES;
				break;
			}
		}
		parent.getSymbol().setLEQ(parentLEQ);
	}
	

}
