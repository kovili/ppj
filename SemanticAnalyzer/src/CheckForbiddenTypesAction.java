
public class CheckForbiddenTypesAction extends Action {
	
	private VarType []forbiddenTypes;


	public CheckForbiddenTypesAction(int childIndex, VarType []forbiddenTypes) {
		super(childIndex);
		this.forbiddenTypes = forbiddenTypes;
	}

	@Override
	void executeAction(TreeNode parent) {
		parent.getChild(getChildIndex()).getSymbol().checkForbiddenTypes(forbiddenTypes);
	}

}
