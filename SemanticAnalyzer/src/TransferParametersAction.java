import java.util.ArrayList;
import java.util.List;

public class TransferParametersAction extends Action {
	
	private int transferParamIndex;

	public TransferParametersAction(int childIndex, int transferParamIndex) {
		super(childIndex);
		this.transferParamIndex = transferParamIndex;
	}

	@Override
	void executeAction(TreeNode parent) {
		List<VarType> argTypes = parent.getChild(getChildIndex()).getSymbol().getArgs();
		List<String> argNames = parent.getChild(getChildIndex()).getSymbol().getNames();
		List<IDN> identifiers = new ArrayList<>();
		
		for(int i = 0; i < argTypes.size(); i++) {
			IDN newIdentifier = new IDN("IDN 0 " + argNames.get(i), new String []{"IDN", "0", argNames.get(i)});
			newIdentifier.setType(argTypes.get(i));
			newIdentifier.initializeName();
			identifiers.add(newIdentifier);
		}
		
		identifiers.forEach((identifier) -> parent.getChild(transferParamIndex).getScope().addIdentifierDeclaration(identifier));
	}

}
