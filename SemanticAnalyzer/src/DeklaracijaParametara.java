
public class DeklaracijaParametara extends NonTerminalSymbol {
	
	private String idnName;

	public DeklaracijaParametara(String name) {
		super(name);
	}

	@Override
	String getNameParameter() {
		return idnName;
	}

	@Override
	public void setNameParameter(String name) {
		this.idnName = name;
	}
	
	
	
	

}
