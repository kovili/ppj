
public class NIZ_ZNAKOVA extends LexicalUnit {

	private String text;
	
	public NIZ_ZNAKOVA(String lexUnit) {
		super(lexUnit);
	}
	
	public NIZ_ZNAKOVA(String lexUnit, String []lexInfo) {
		super(lexUnit, lexInfo);
		setType(VarType.CONST_CHAR_ARRAY);
		setLEQ(LExpressionQualifier.NO);
		this.text = lexInfo[2].substring(1, lexInfo[2].length() - 1);
		this.text = lexInfo[2].endsWith(SemanticUtils.CONST_CHAR_END) ? this.text : this.text + SemanticUtils.CONST_CHAR_END;
	}
	
	@Override
	void checkParameter() {
		if(!SemanticUtils.validEscaping(text.toCharArray())) {
			signalError();
		}
	}

	@Override
	public int getArraySize() {
		return text.length();
	}

	@Override
	public int stringSizeRequest() {
		return text.length();
	}



	
	
	
	

}
