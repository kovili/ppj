
public class CheckRequiredTypesAction extends Action {
	
	private VarType []requiredTypes;

	public CheckRequiredTypesAction(int childIndex, VarType []requiredTypes) {
		super(childIndex);
		this.requiredTypes = requiredTypes;
	}

	@Override
	void executeAction(TreeNode parent) {
		parent.getChild(getChildIndex()).getSymbol().checkRequiredTypes(requiredTypes);
	}

}
