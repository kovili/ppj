
public class CheckExplicitCastAction extends Action {
	
	private int rightCandidateIndex;

	public CheckExplicitCastAction(int childIndex, int rightCandidateIndex) {
		super(childIndex);
		this.rightCandidateIndex = rightCandidateIndex;
	}

	@Override
	void executeAction(TreeNode parent) {
		if(!SemanticUtils.checkCastCompatibility(parent.getChild(getChildIndex()).getSymbol().getType(),parent.getChild(rightCandidateIndex).getSymbol().getType())) {
			parent.getChild(getChildIndex()).getSymbol().signalError();
		}
	}

}
