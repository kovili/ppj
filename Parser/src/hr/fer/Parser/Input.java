package hr.fer.Parser;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

import hr.fer.Parser.Symbol.NonTerminalSymbol;
import hr.fer.Parser.Symbol.Production;
import hr.fer.Parser.Symbol.TerminalSymbol;
import hr.fer.Parser.ENFA.*;
import hr.fer.Parser.DFA.*;

/**
 * Parser generator.<br>
 * Parses input file for parser definition defined by:
 * 	<ul>
 * 		<li><code>Non terminal symbols</code> NTSymbol1 NTSymbol2 ... NTSymbolN</li>
 * 		<li><code>Terminal symbols</code> TSymbol1 TSymbol2 ... TSymbolN</li>
 * 		<li><code>Sync symbols</code> SyncSym1 SyncSym2 ... SyncSymN</li>
 * 		<li><code>Grammar productions</code>
 * </ul>
 * 
 * After extracting parser definition, the parser generator:
 * <ol>
 * 		<li>Currently only prints out the productions it parsed</li>
 * </ol>
 */
public class Input {

	public static void main(String[] args)throws IOException {
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		String line;
		ArrayList<NonTerminalSymbol> nonTerminalSymbols = new ArrayList<NonTerminalSymbol>();
		ArrayList<TerminalSymbol> terminalSymbols = new ArrayList<TerminalSymbol>();
		ArrayList<TerminalSymbol> synchronisationSymbols = new ArrayList<TerminalSymbol>();
		ArrayList<String> productionList = new ArrayList<String>();
		
		
		line = reader.readLine();
		Util.extractNonTerminalSymbols(nonTerminalSymbols, line);
		line = reader.readLine();
		Util.extractTerminalSymbols(terminalSymbols, line);
		line = reader.readLine();
		Util.extractTerminalSymbols(synchronisationSymbols, line);
		
		while(reader.ready()) {
			line=reader.readLine();
			productionList.add(line);
		}
		reader.close();
		for(int i=0;(i+1)<productionList.size();) {
			if(productionList.get(i).startsWith("<")) {
				NonTerminalSymbol leftSide = new NonTerminalSymbol(productionList.get(i));
				i++;
				while(i < productionList.size() && productionList.get(i).startsWith(" ")) {
					Production rightSide = new Production();
					String []definition = productionList.get(i).substring(1).split(" ");
					for (String unit:definition) {
						if (unit.startsWith("<")) {
							for(NonTerminalSymbol n:nonTerminalSymbols) {
								if(n.getSymbolName().equals(unit)) rightSide.addSymbol(n);
							}
						}else {
							TerminalSymbol symbol = new TerminalSymbol(unit);
							rightSide.addSymbol(symbol);
						}
						for(NonTerminalSymbol n:nonTerminalSymbols) {
							if(n.equals(leftSide)) leftSide=n;
						}
					}
					leftSide.addProduction(rightSide);
					i++;
				}
			}
		}
		
		//Util.printProductions(nonTerminalSymbols);
		
		nonTerminalSymbols.forEach(symbol->symbol.processBeginsDirectlyWith());
		//Util.printBeginsDirectlyWith(nonTerminalSymbols);
		
		for(NonTerminalSymbol symbol:nonTerminalSymbols) {
			Production p=Util.processNonTerminalSymbol(symbol);
			symbol.setBeginsWith(p);
		}
		//Util.printBeginsWith(nonTerminalSymbols);
		
		nonTerminalSymbols.forEach(symbol->symbol.processBegins());
		//Util.printBegins(nonTerminalSymbols);
		
		ENFA enfa = new ENFA(nonTerminalSymbols);
		//enfa.printENFA();
		DFA dfa = new DFA(nonTerminalSymbols, terminalSymbols, enfa.getStates());
		//dfa.printDFA();
		Tables table = new Tables(dfa.getDfaStates());
		//table.printAction();
		//table.printnewState();
		
		Parser.build(table.getAction(),table.getNewState(),synchronisationSymbols);
	}

}
