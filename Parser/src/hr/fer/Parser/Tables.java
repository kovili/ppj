package hr.fer.Parser;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

//import hr.fer.Parser.*;
import hr.fer.Parser.DFA.*;
import hr.fer.Parser.ENFA.*;
import hr.fer.Parser.Symbol.*;

public class Tables implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private ArrayList<DFAState> dfaStates = new ArrayList<>();
	
	private HashMap<TableKey, String> action = new HashMap<>(); //akcija i novo stanje
	private HashMap<TableKey, String> newState = new HashMap<>();
	
	
	
	public Tables(ArrayList<DFAState> dfaStates) {
		this.dfaStates=dfaStates;
		buildAction();
	}
	
	public void buildAction() {
		for(DFAState currentState : dfaStates) {
			for(ENFAState enfaState : currentState.getEnfaStates()) {
				if(enfaState.getLeftSide().getSymbolName().equals("<S''>") && enfaState.getDotPosition()==1) { //Prihvati()
					action.put(new TableKey(currentState.getStateNumber(), "#"), "Prihvati");
				}
				
				else {
					if(enfaState.getRightSide().size() != enfaState.getDotPosition()) {
						
						if(enfaState.getRightSide().get(enfaState.getDotPosition()).getSymbolType().equals("terminal") && 
						!enfaState.getRightSide().get(enfaState.getDotPosition()).getSymbolName().equals("$")) {  //Pomakni(i)
							TerminalSymbol ts = (TerminalSymbol) enfaState.getRightSide().get(enfaState.getDotPosition());
							int i = currentState.getTransitions().get(ts).getStateNumber();
							TableKey tk = new TableKey(currentState.getStateNumber(), ts.getSymbolName());
							action.put(tk, "Pomakni "+i);
						}
						
						else if(enfaState.getRightSide().get(enfaState.getDotPosition()).getSymbolName().equals("$")) {  //Reduciraj  $  element_koji_se_dodaje
							//System.out.println("");
							String str = "Reduciraj" + enfaState.getRightSide().toString() + ":" + enfaState.getLeftSide();
							for(Symbol s : enfaState.getTerminalSymbols().getProduction()) {
								s=(TerminalSymbol) s;
								action.put(new TableKey(currentState.getStateNumber(), s.getSymbolName()), str);
							}
						}
						
						else if(enfaState.getRightSide().get(enfaState.getDotPosition()).getSymbolType().equals("nonterminal")) { //Stavi(i)
							NonTerminalSymbol nts = (NonTerminalSymbol) enfaState.getRightSide().get(enfaState.getDotPosition());
							int i = currentState.getTransitions().get(nts).getStateNumber();
							newState.put(new TableKey(currentState.getStateNumber(), nts.getSymbolName()), "Stavi "+i);
						}
						
					}
					else { 
						for(Symbol s : enfaState.getTerminalSymbols().getProduction()) {  //Reduciraj   elementi_koji se mijenjaju   element_koji_ih_mijenja
							s=(TerminalSymbol) s;
							String str = "Reduciraj" + enfaState.getRightSide().toString() + ":" + enfaState.getLeftSide();
							TableKey tk = new TableKey(currentState.getStateNumber(), s.getSymbolName());
							if(!action.containsKey(tk)) {
								action.put(tk, str);
							}
						}
					}
				}
			}
		}
	}
	

	public void printAction() {
		System.out.println("Akcija: ");
		for(TableKey tk : action.keySet()) {
			System.out.println(tk.toString()+" : "+action.get(tk));
		}
		System.out.println("\n");
	}
	
	public void printnewState() {
		System.out.println("Novo stanje: ");
		for(TableKey tk : newState.keySet()) {
			System.out.println(tk.toString()+" : "+newState.get(tk));
		}
		System.out.println("\n");
	}
	
	public HashMap<TableKey, String> getAction(){
		return action;
	}
	public HashMap<TableKey, String> getNewState(){
		return newState;
	}
	
}
