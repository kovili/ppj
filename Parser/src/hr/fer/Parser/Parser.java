package hr.fer.Parser;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import hr.fer.Parser.Symbol.*;

public class Parser implements Serializable{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static void build(HashMap<TableKey, String> action ,HashMap<TableKey, String> newState, ArrayList<TerminalSymbol> synchronisationSymbols) throws IOException{
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		String line;
		ArrayList<String> inputList = new ArrayList<String>();
		String currentInput = new String();
		int currentStateNumber;
 		ArrayList<StackElement> stack = new ArrayList<>();
		int i=0;
		
		while(reader.ready()) {
			line=reader.readLine();
			inputList.add(line);
		}
		reader.close();
		inputList.add("#");
		
		StackElement stackBottom= new StackElement("bottom" , 0);
		stack.add(stackBottom);
		
		while(true) {
			currentInput=inputList.get(i).split(" ")[0];
			currentStateNumber=stack.get(stack.size()-1).getStateNumber();
			TableKey tk = new TableKey(currentStateNumber, currentInput);
			
			if(!action.containsKey(tk)) { //oporavak od pogre�ke
				while (i<inputList.size()) {
					if(synchronisationSymbols.contains(new TerminalSymbol(inputList.get(i).split(" ")[0]))) {
						currentInput=inputList.get(i).split(" ")[0];
						while(!action.containsKey(new TableKey(stack.get(stack.size()-1).getStateNumber(), currentInput)) || stack.isEmpty()) {
							stack.remove(stack.size()-1);
						}
						break;
					}
					i++;
				}
				if(!stack.isEmpty() && i<inputList.size())
					continue;
				else break;
			}
			
			if(action.get(tk).startsWith("Pomakni")) {
				StackElement leaf = new StackElement(inputList.get(i), Integer.parseInt(action.get(tk).split(" ")[1])); //(data, stateNumber)
				stack.add(leaf);
				i++;
			}
			
			else if(action.get(tk).startsWith("Reduciraj")) {
				String s = action.get(tk).substring(10);
				String rightSide =s.split(":")[0];
				String reduceState = s.split(":")[1];
				if(rightSide.equals("$")) {
					TableKey tkns = new TableKey(stack.get(stack.size()-1).getStateNumber(), reduceState);
					StackElement leaf = new StackElement(reduceState, Integer.parseInt(newState.get(tkns).split(" ")[1]));
					leaf.addChild(new StackElement("$",0));
					stack.add(leaf);
				}
				else {
					ArrayList<StackElement> helperList = new ArrayList<>();
					for(String str : rightSide.split(" ")) {
						helperList.add(stack.get(stack.size()-1));
						stack.remove(stack.size()-1);
					}
					TableKey tkns = new TableKey(stack.get(stack.size()-1).getStateNumber(), reduceState);
					StackElement node = new StackElement(reduceState, Integer.parseInt(newState.get(tkns).split(" ")[1]));
					for(StackElement e:helperList) {
						node.getChildren().add(0, e);
					}
					stack.add(node);
				}
			}
			
			else if(action.get(tk).startsWith("Prihvati")) {
				//System.out.println("Prihvaceno");
				break;
			}
		}
		
		finalPrint(stack.get(stack.size()-1),0);
	}
	
	public static void finalPrint(StackElement element, int depth) {
		String s = new String();
		for(int i=0; i<depth;i++) {
			s+=" ";
		}
		System.out.println(s+element.getData());
		for(StackElement e : element.getChildren()) {
			finalPrint(e,depth+1);
		}
		return;
	}
	
	
}
