package hr.fer.Parser;

import java.io.Serializable;
import java.util.ArrayList;

public class StackElement implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String data=new String();
	private ArrayList<StackElement> children = new ArrayList<>();
	private int stateNumber;
	
	public StackElement(String data, int stateNumber) {
		this.data=data;
		this.stateNumber=stateNumber;
	}
	
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
	public ArrayList<StackElement> getChildren() {
		return children;
	}
	public void addChild(StackElement child) {
		this.children.add(child);
	}
	public int getStateNumber() {
		return stateNumber;
	}
	public void setStateNumber(int stateNumber) {
		this.stateNumber = stateNumber;
	}
	
	
	
	
}
