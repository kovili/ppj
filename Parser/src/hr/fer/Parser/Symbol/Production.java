package hr.fer.Parser.Symbol;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * A production from a non terminal symbol.
 */
public class Production implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private ArrayList<Symbol> production = new ArrayList<>();
	
	public void addSymbol(Symbol symbol) {
		this.production.add(symbol);
	}
	
	public Symbol get(int index) {
		return production.get(index);
	}
	
	public void addProduction(Production p) {
		for(Symbol s:p.getProduction()) {
			if(!this.production.contains(s))
				this.production.add(s);
		}
	}
	
	public ArrayList<Symbol> getProduction(){
		return production;
	}
	
	public boolean contains(Symbol symbol) {
		for(Symbol s:production) {
			if(s.equals(symbol)) return true;
		}
		return false;
	}
	
	public int size() {
		return production.size();
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((production == null) ? 0 : production.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Production other = (Production) obj;
		if (production == null) {
			if (other.production != null)
				return false;
		} else if (!production.equals(other.production))
			return false;
		return true;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		production.forEach((symbol) -> sb.append(" " + symbol));
		return sb.toString();
	}
	
}
