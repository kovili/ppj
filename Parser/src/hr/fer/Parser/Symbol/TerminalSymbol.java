package hr.fer.Parser.Symbol;

public class TerminalSymbol implements Symbol{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String symbolName;
	
	public TerminalSymbol(String symbolName) {
		this.symbolName=symbolName;
	}
	
	public String getSymbolName() {
		return symbolName;
	}
	
	public String getSymbolType() {
		return "terminal";
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TerminalSymbol other = (TerminalSymbol) obj;
		if (symbolName == null) {
			if (other.symbolName != null)
				return false;
		} else if (!symbolName.equals(other.symbolName))
			return false;
		return true;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((symbolName == null) ? 0 : symbolName.hashCode());
		return result;
	}
	
	@Override
	public String toString() {
		return symbolName;
	}

	@Override
	public boolean hasEpsilon() {
		// TODO Auto-generated method stub
		return false;
	}

	
}
