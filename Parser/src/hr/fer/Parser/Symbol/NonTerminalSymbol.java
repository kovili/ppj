package hr.fer.Parser.Symbol;

import java.util.ArrayList;

public class NonTerminalSymbol implements Symbol{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**The nonterminal symbol name is enclosed in triangular parentheses <>.*/
	private String symbolName;
	private ArrayList<Production> productions = new ArrayList<>();
	//ZapocinjeIzravnoZnakom
	private Production beginsDirectlyWith = new Production();
	//ZapocinjeZnakom
	private Production beginsWith = new Production();
	//Zapocinje
	private Production begins = new Production();
	
	public String getSymbolName() {
		return symbolName;
	}
	
	public void setSymbolName(String symbolName) {
		this.symbolName=symbolName;
	}
	public String getSymbolType() {
		return "nonterminal";
	}
	
	public NonTerminalSymbol(String symbolName) {
		this.symbolName=symbolName;
	}
	
	public void addProduction(Production production) {
		this.productions.add(production);
	}
	
	public Production getProduction(int position) {
		return this.productions.get(position);
	}
	
	public ArrayList<Production> getProductions() {
		return this.productions;
	}
	
	public void printProductions() {
		System.out.println(this.symbolName);
		for (Production p:productions) {
			System.out.println(" "+ p.toString());
		}
	}
	
	public boolean hasEpsilon() {
		for(Production p:productions) {
			if(p.get(0).getSymbolName().equals("$")) return true;
		}
		return false;
	}
	
	public Production getBeginsDirectlyWith() {
		return beginsDirectlyWith;
	}
	
	public Production getBeginsWith() {
		return beginsWith;
	}
	public void setBeginsWith(Production beginsWith) {
		this.beginsWith=beginsWith;
	}
	
	public Production getBegins() {
		return begins;
	}
	
	public void processBeginsDirectlyWith() {
		for(Production p:productions) {
			for(int i=0;i<p.getProduction().size();i++) {
				if(!p.get(i).getSymbolName().equals("$") && !beginsDirectlyWith.contains(p.get(i))) {
					beginsDirectlyWith.addSymbol(p.get(i));
				}else break;
				if(p.get(i).getSymbolType().equals("nonterminal")) {
					if(p.get(i).hasEpsilon()) {
						continue;
					}else break;
				}else break;
				
			}
		}
	}
	
	public void processBegins() {
		for(Symbol s : beginsWith.getProduction()) {
			if(!s.getSymbolName().startsWith("<")) begins.addSymbol(s);
		}
	}
	
	@Override

	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((symbolName == null) ? 0 : symbolName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		NonTerminalSymbol other = (NonTerminalSymbol) obj;
		if (symbolName == null) {
			if (other.symbolName != null)
				return false;
		} else if (!symbolName.equals(other.symbolName))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return symbolName;
	}
	
	
}

