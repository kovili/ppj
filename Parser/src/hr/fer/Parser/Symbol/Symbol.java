package hr.fer.Parser.Symbol;

import java.io.Serializable;

/**
 * A non-terminal or terminal symbol.
 */
public interface Symbol extends Serializable{
	/**
	 * Returns "terminal" if terminal symbol and "nonterminal" if nonterminal symbol.
	 */
	String getSymbolType();
	
	String getSymbolName();

	boolean hasEpsilon();
}
