package hr.fer.Parser;

import java.io.Serializable;

public class TableKey implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int stateNumber;
	private String transitionSymbol;
	
	public TableKey(int stateNumber, String transitionSymbol) {
		this.stateNumber=stateNumber;
		//if(transitionSymbol.getSymbolType().equals("terminal")) {
		//	this.transitionSymbol=(TerminalSymbol) transitionSymbol;
		//}
		//else {
		//	this.transitionSymbol=(NonTerminalSymbol) transitionSymbol;
		//}
		this.transitionSymbol=transitionSymbol;
	}

	@Override
	public String toString() {
		return "("+ stateNumber + "," + transitionSymbol + ")";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + stateNumber;
		result = prime * result + ((transitionSymbol == null) ? 0 : transitionSymbol.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TableKey other = (TableKey) obj;
		if (stateNumber != other.stateNumber)
			return false;
		if (transitionSymbol == null) {
			if (other.transitionSymbol != null)
				return false;
		} else if (!transitionSymbol.equals(other.transitionSymbol))
			return false;
		return true;
	}
	
	
}
