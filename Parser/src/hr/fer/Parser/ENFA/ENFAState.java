package hr.fer.Parser.ENFA;

import hr.fer.Parser.Symbol.*;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

public class ENFAState implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private NonTerminalSymbol leftSide;//lijeva strana produkcije LR(1) stavke
	private int productionPosition; //indeks desne strane produkcije LR(1) stavke, desna strana se nalazi unutar nezavr�nog znaka leftSide
	private int dotPosition;//pozicija to�ke unutar produkcije LR(1) stavke
	private Production terminalSymbols = new Production();//lista zavr�nih znakova LR(1) stavke
	
	private ArrayList<ENFAState> loopControl = new ArrayList<>();
	
	HashMap<Symbol, ArrayList<ENFAState>> transitions = new HashMap<>();
	
	public void addTransition(Symbol transitionSymbol, ENFAState transitionState) {
		if(!transitions.containsKey(transitionSymbol)) {
			ArrayList<ENFAState>  states= new ArrayList<>();
			states.add(transitionState);
			this.transitions.put(transitionSymbol, states);
		}
		else {
			this.transitions.get(transitionSymbol).add(transitionState);
		}
	}
	
	
	public ArrayList<ENFAState> epsilonTransitionCalculator(){
		//ArrayList<ENFAState> list = new ArrayList<>();
		TerminalSymbol s=new TerminalSymbol("$");
		if(transitions.containsKey(s)) {
			for(ENFAState e : transitions.get(s)) {
					//if(e.getLeftSide().equals(leftSide) && e.getProductionPosition()==productionPosition &&
					//		e.getDotPosition()==dotPosition && e.getTerminalSymbols().equals(terminalSymbols)) {//epsilon loop problemi
						//continue;
					//}
				if(!loopControl.contains(e)) {
						//list.add(e);
					loopControl.add(e);
					ArrayList<ENFAState> helperList = e.epsilonTransitionCalculator();
					for (ENFAState state : helperList) {
						if(!loopControl.contains(state)) {
							loopControl.add(state);
						}
					}
				}
			}
		}
		return loopControl;
	}
	public ArrayList<ENFAState> epsilonTransitions(){
		if(!loopControl.isEmpty()) return loopControl;
		else return epsilonTransitionCalculator();
	}
	
	public NonTerminalSymbol getLeftSide() {
		return leftSide;
	}

	public void setLeftSide(NonTerminalSymbol leftSide) {
		this.leftSide = leftSide;
	}
	public int getProductionPosition() {
		return productionPosition;
	}
	public void setProductionPosition(int productionPosition) {
		this.productionPosition = productionPosition;
	}
	public int getDotPosition() {
		return dotPosition;
	}
	public void setDotPosition(int dotPosition) {
		this.dotPosition = dotPosition;
	}
	public Production getTerminalSymbols() {
		return terminalSymbols;
	}
	public void addTerminalSymbols(Production terminalSymbols) {
		this.terminalSymbols.addProduction(terminalSymbols);
	}
	public Production getRightSide() {
		return leftSide.getProduction(productionPosition);
	}
	

	public HashMap<Symbol, ArrayList<ENFAState>> getTransitions() {
		return transitions;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + dotPosition;
		result = prime * result + ((leftSide == null) ? 0 : leftSide.hashCode());
		result = prime * result + productionPosition;
		result = prime * result + ((terminalSymbols == null) ? 0 : terminalSymbols.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ENFAState other = (ENFAState) obj;
		if (dotPosition != other.dotPosition)
			return false;
		if (leftSide == null) {
			if (other.leftSide != null)
				return false;
		} else if (!leftSide.equals(other.leftSide))
			return false;
		if (productionPosition != other.productionPosition)
			return false;
		if (terminalSymbols == null) {
			if (other.terminalSymbols != null)
				return false;
		} else if (!terminalSymbols.equals(other.terminalSymbols))
			return false;
		return true;
	}

	public void printTransitions() {
		for(Symbol s : transitions.keySet()) {
			System.out.println(s.getSymbolName()+" : ");
			for(ENFAState e:transitions.get(s)) {
				System.out.println("  "+e.toString());
			}
		}
		System.out.println("\n");
	}
	@Override
	public String toString() {
		return leftSide + " ->" + leftSide.getProduction(productionPosition).toString() + ", dotPosition =" + dotPosition + ",{" + terminalSymbols + "}";
	}
	
	
	
}
