package hr.fer.Parser.ENFA;

import java.io.Serializable;
import java.util.ArrayList;

import hr.fer.Parser.Symbol.NonTerminalSymbol;
import hr.fer.Parser.Symbol.Production;
import hr.fer.Parser.Symbol.TerminalSymbol;
import hr.fer.Parser.Util;

public class ENFA implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private ArrayList<ENFAState> states = new ArrayList<>();
	//private ArrayList<NonTerminalSymbol> nonTerminalSymbols;
	private ENFAState beginState;
	TerminalSymbol epsilon = new TerminalSymbol("$");
	
	public ENFA(ArrayList<NonTerminalSymbol> nonTerminalSymbols) {
		NonTerminalSymbol newBeginning = new NonTerminalSymbol("<S''>");
		Production p = new Production();
		p.addSymbol(nonTerminalSymbols.get(0));
		newBeginning.addProduction(p);
		
		ENFAState state = new ENFAState();
		state.setLeftSide(newBeginning);
		state.setDotPosition(0);
		state.setProductionPosition(0);
		TerminalSymbol endOfLine = new TerminalSymbol("#");
		Production terminalSymbols = new Production();
		terminalSymbols.addSymbol(endOfLine);
		state.addTerminalSymbols(terminalSymbols);
		this.beginState=state;
		this.states.add(beginState);
		buildStates();
	}
	
	public void buildStates() {
		buildState(this.beginState);
	}
	
	
	public void buildState(ENFAState state) {
		//if(state.getLeftSide().getProductions().isEmpty()) return; 
		if(state.getDotPosition()==state.getLeftSide().getProduction(state.getProductionPosition()).size()) {
			return;
		}
		if(state.getLeftSide().getProduction(state.getProductionPosition()).get(0).getSymbolName().equals("$")) {
			return;
		}
		
		NonTerminalSymbol leftSide=state.getLeftSide();
		Production rightSide = state.getLeftSide().getProduction(state.getProductionPosition());
		int dotPosition = state.getDotPosition();
		Production terminalSymbols = state.getTerminalSymbols();
		
		ENFAState stateB=new ENFAState(); //nazvano stateB radi algoritma koji je opisan u stavci 4) b) u knjizi na strani 148.
		stateB.setLeftSide(leftSide);
		stateB.setProductionPosition(state.getProductionPosition());
		stateB.setDotPosition(dotPosition+1);
		stateB.addTerminalSymbols(terminalSymbols);
		state.addTransition(rightSide.get(dotPosition), stateB);
		this.states.add(stateB);
		buildState(stateB); //rekurzivni poziv funkcije
		
		if(rightSide.get(dotPosition).getSymbolType().equals("nonterminal")) { //znak desno od to�ke je nezavr�ni
			Production futureTerminalSymbols = new Production();
			if(dotPosition + 1 == rightSide.size()) {
				futureTerminalSymbols.addProduction(terminalSymbols);
			}
			else {
				futureTerminalSymbols.addProduction(Util.begins(rightSide.getProduction().subList(dotPosition+1, rightSide.size())));
				if(Util.isEmpty(rightSide.getProduction().subList(dotPosition+1, rightSide.size()))) {
					futureTerminalSymbols.addProduction(terminalSymbols);
				}
			}
			NonTerminalSymbol newLeftSide = (NonTerminalSymbol) rightSide.get(dotPosition);
			
			for(int i=0; i<newLeftSide.getProductions().size();i++) {
				ENFAState stateC = new ENFAState(); //nazvano stateC radi algoritma koji je opisan u stavci 4) c) u knjizi na strani 148.
				stateC.setLeftSide(newLeftSide);
				stateC.setProductionPosition(i);
				stateC.setDotPosition(0);
				stateC.addTerminalSymbols(futureTerminalSymbols);
				int built=0;
				for (ENFAState s:states) {
					if(stateC.equals(s)) {
						stateC=s;
						built=1;
						break;
					}
				}
				state.addTransition(epsilon, stateC);
				if(built==0 && !states.contains(stateC)) {
					this.states.add(stateC);
					buildState(stateC); //rekurzivni poziv funkcije
				}
			}
		}
	}
	
	public ArrayList<ENFAState> getStates(){
		return states;
	}

	@Override
	public String toString() {
		return "ENFA: \n"+ states;
	}
	
	public void printENFA() {
		for(ENFAState e : states) {
			System.out.println(e.toString());
			//e.printTransitions();
		}
	}
}
