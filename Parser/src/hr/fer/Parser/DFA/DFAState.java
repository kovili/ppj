package hr.fer.Parser.DFA;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import hr.fer.Parser.ENFA.ENFAState;
import hr.fer.Parser.Symbol.*;


public class DFAState implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int stateNumber;
	private ArrayList<ENFAState> enfaStates = new ArrayList<>();
	private HashMap<Symbol, DFAState> transitions = new HashMap<>();
	
	
	public int getStateNumber() {
		return stateNumber;
	}
	public void setStateNumber(int stateNumber) {
		this.stateNumber = stateNumber;
	}
	public ArrayList<ENFAState> getEnfaStates() {
		return enfaStates;
	}
	public void addEnfaStates(ArrayList<ENFAState> enfaStates) {
		for(ENFAState e : enfaStates) {
			if(!this.enfaStates.contains(e)) this.enfaStates.add(e);
		}
	}
	
	public void addEnfaState(ENFAState enfaState) {
		if(!this.enfaStates.contains(enfaState))
			this.enfaStates.add(enfaState);
	}
	
	public void addTransition(Symbol s, DFAState d) {
		this.transitions.put(s, d);
	}
	public HashMap<Symbol, DFAState> getTransitions(){
		return this.transitions;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((enfaStates == null) ? 0 : enfaStates.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DFAState other = (DFAState) obj;
		if (enfaStates == null) {
			if (other.enfaStates != null)
				return false;
		} else if (!enfaStates.equals(other.enfaStates))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "stateNumber=" + stateNumber + "\n " + enfaStates;
	}
	
	
}
