package hr.fer.Parser.DFA;

import java.io.Serializable;
import java.util.ArrayList;

import hr.fer.Parser.ENFA.ENFAState;
import hr.fer.Parser.Symbol.*;

public class DFA implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private ArrayList<NonTerminalSymbol> nonTerminalSymbols = new ArrayList<>();
	private ArrayList<TerminalSymbol> terminalSymbols = new ArrayList<>();
	
	private ArrayList<DFAState> dfaStates = new ArrayList<>();
	
	private int stateCounter = 1;
	private int dfaIterator = 0;
	
	public DFA(ArrayList<NonTerminalSymbol> nonTerminalSymbols, ArrayList<TerminalSymbol> terminalSymbols, ArrayList<ENFAState> states) {
		this.nonTerminalSymbols=nonTerminalSymbols;
		this.terminalSymbols=terminalSymbols;
		
		DFAState beginningState = new DFAState();
		beginningState.setStateNumber(0);
		beginningState.addEnfaState(states.get(0));
		beginningState.addEnfaStates(states.get(0).epsilonTransitions());
		this.dfaStates.add(beginningState);
		buildDFA();
		
	}
	
	
	
	
	
	public void buildDFA() {
		while(dfaIterator<stateCounter) {
			DFAState currentState = dfaStates.get(dfaIterator);
			//paste here
			for(NonTerminalSymbol nts : nonTerminalSymbols) {
				DFAState newState = new DFAState();
				for(ENFAState es : currentState.getEnfaStates()) {
					if(es.getTransitions().containsKey(nts)) {
						if(!newState.getEnfaStates().contains(es.getTransitions().get(nts).get(0))) {
							newState.addEnfaState(es.getTransitions().get(nts).get(0));
							newState.addEnfaStates(es.getTransitions().get(nts).get(0).epsilonTransitions());
						}
					}
				}
				if(newState.getEnfaStates().isEmpty()) continue;
				if(dfaStates.contains(newState)) {
					for (DFAState d : dfaStates) {
						if (d.equals(newState)) newState = d;
					}
					currentState.addTransition(nts, newState);
				}
				else {
					newState.setStateNumber(stateCounter);
					stateCounter++;
					currentState.addTransition(nts, newState);
					dfaStates.add(newState);
				}
			}
			
			for(TerminalSymbol ts : terminalSymbols) {
				DFAState newState = new DFAState();
				for(ENFAState es : currentState.getEnfaStates()) {
					if(es.getTransitions().containsKey(ts)) {
						if(!newState.getEnfaStates().contains(es.getTransitions().get(ts).get(0))) {
							newState.addEnfaState(es.getTransitions().get(ts).get(0));
							newState.addEnfaStates(es.getTransitions().get(ts).get(0).epsilonTransitions());
						}
					}
				}
				if(newState.getEnfaStates().isEmpty()) continue;
				if(dfaStates.contains(newState)) {
					for (DFAState d : dfaStates) {
						if (d.equals(newState)) newState = d;
					}
					currentState.addTransition(ts, newState);
				}
				else {
					newState.setStateNumber(stateCounter);
					stateCounter++;
					currentState.addTransition(ts, newState);
					dfaStates.add(newState);
				}
			}
			//paste here
			dfaIterator++;
		}
	}
	
	
	
	
	
	public void printDFA() {
		for(DFAState d : dfaStates) {
			System.out.println(d.toString());
			for(Symbol s : d.getTransitions().keySet()) {
				System.out.println(" "+s.getSymbolName()+"->"+d.getTransitions().get(s).getStateNumber());
			}
		}
	}

	public ArrayList<NonTerminalSymbol> getNonTerminalSymbols() {
		return nonTerminalSymbols;
	}

	public ArrayList<TerminalSymbol> getTerminalSymbols() {
		return terminalSymbols;
	}

	public ArrayList<DFAState> getDfaStates() {
		return dfaStates;
	}
	
	
}
