package hr.fer.Parser;

import java.util.ArrayList;
import java.util.List;

import hr.fer.Parser.Symbol.*;

/**
 * Helper class with useful methods.
 */
public class Util {

	/**
	 * Given line in format: <br>
	 * %V NTSym1 NTSym2 ... NTSymN, <br>
	 * Extracts all NTSyms into a list.
	 */
	public static void extractNonTerminalSymbols(ArrayList<NonTerminalSymbol> nonTerminalSymbols, String definition) {
		String []symbols = definition.split(" ");
		for(int i = 1; i < symbols.length; i++) {
			NonTerminalSymbol symbol = new NonTerminalSymbol(symbols[i]);
			nonTerminalSymbols.add(symbol);
		}
		
	}

	/**
	 * Given line in format: <br>
	 * %V TSym1 TSym2 ... TSymN, <br>
	 * Extracts all TSyms into a list.
	 */
	public static void extractTerminalSymbols(ArrayList<TerminalSymbol> terminalSymbols, String definition) {
		String []symbols = definition.split(" ");
		for(int i = 1; i < symbols.length; i++) {
			TerminalSymbol symbol = new TerminalSymbol(symbols[i]);
			terminalSymbols.add(symbol);
		}
		
	}
	
	
	public static void printProductions(ArrayList<NonTerminalSymbol> nonTerminalSymbols) {
		System.out.println("Produkcije:");
		for(NonTerminalSymbol s:nonTerminalSymbols) {
			s.printProductions();
			System.out.println("");
		}
	}
	public static void printBeginsDirectlyWith(ArrayList<NonTerminalSymbol> nonTerminalSymbols) {
		System.out.println("ZapočinjeIzravnoZnakom:");
		for(NonTerminalSymbol s:nonTerminalSymbols) {
			System.out.println(s.getSymbolName());
			System.out.println(s.getBeginsDirectlyWith().toString());
			System.out.println("");
		}
	}
	
	public static Production processNonTerminalSymbol(NonTerminalSymbol symbol) {
		Production beginsDirectlyWith = symbol.getBeginsDirectlyWith();
		Production beginsWith = new Production();
		beginsWith.addSymbol(symbol);
		
		for(Symbol s:beginsDirectlyWith.getProduction()) {
			if(!s.getSymbolName().equals("$")) {
				if(s.getSymbolType().equals("nonterminal") && !beginsWith.contains(s)) {
					Production p=processNonTerminalSymbol((NonTerminalSymbol)s);
					p.getProduction().forEach(sym->beginsWith.addSymbol(sym));
				}else beginsWith.addSymbol(s);
			}
		}
		return beginsWith;
	}

	public static void printBeginsWith(ArrayList<NonTerminalSymbol> nonTerminalSymbols) {
		System.out.println("ZapočinjeZnakom:");
		for(NonTerminalSymbol s:nonTerminalSymbols) {
			System.out.println(s.getSymbolName());
			System.out.println(s.getBeginsWith().toString());
			System.out.println("");
		}
	}
	
	public static void printBegins(ArrayList<NonTerminalSymbol> nonTerminalSymbols) {
		System.out.println("Započinje:");
		for(NonTerminalSymbol s:nonTerminalSymbols) {
			System.out.println(s.getSymbolName());
			System.out.println(s.getBegins().toString());
			System.out.println("");
		}
	}
	
	public static Production begins(List<Symbol> list) {
		Production beginList = new Production();
		for(Symbol s:list) {
			if(s.getSymbolType().equals("nonterminal")) {
				beginList.addProduction(((NonTerminalSymbol) s).getBegins());
				if(!s.hasEpsilon()) break;
			}
			else if(s.getSymbolType().equals("terminal")) {
				if(!beginList.getProduction().contains(s)) {
					beginList.addSymbol(s);
				}
				break;
			}
		}
		return beginList;
	}
	
	public static boolean isEmpty(List<Symbol> list) {
		for(Symbol s:list) {
			if(s.getSymbolType().equals("terminal")) return false;
			if(!s.hasEpsilon()) return false;
		}
		return true;
	}
	
	
	
	
	
	
}
