ppj:
Input - Zasada ulazna to�ka cijelom programu
Util - Pomo�na klasa za izvla�enje podatak iz ulazne datoteke

actions:
Action - Apstraktna akcija koju Lexer moze izvesti
GoBack - Ekv. s VRATI_SE n
NewLine - Ekv. s NOVI_REDAK
RememberLexicalUnit - Prvi redak svake akcije (ako pi�e "-" ova akcija se ne izvodi, ako pi�e uniformni znak, akcija se izvodi)
SwitchState - Ekv. s UDJI_U_STANJE

lexer:
Lexer - Well, it's lexin
LexerRule - Opisuje jedno pravilo iz ulazne datoteke. Oblika je 1) Epsilon NKA koji je generiran iz regexa i 2) Liste akcija koje se izvedu ako se ovo pravilo izabere
LexicalUnit - Leksicka jedinka/Uniformni znak

nfa:
ENFAState - Jedno stanje E-NKA. U njemu su definirani prijelazi za odredena slova u sljedeca stanja
Epsilon_NFA - Enka koji sadrzi sva stanja i sposoban je na zahtjev uci u nova stanja iz pocetnog
EpsilonNFAGeneratora - Das mu regex i on stvori E-NKA
StatePair - Par stanja. Potrebno za algoritam generiranja.