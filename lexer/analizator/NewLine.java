package analizator;
/**
 * Increments line count in the lexer.
 */
public class NewLine implements Action {
	private static final long serialVersionUID = 3621333732441205061L;

	@Override
	public void doAction(Lexer lexer) {
		lexer.incrementLine();
	}

}
