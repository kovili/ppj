package analizator;

/**
 * Changes the current lexer state.
 */
public class SwitchState implements Action {
	private static final long serialVersionUID = 7188774235885025514L;
	
	/**State to which the current state will be switched.*/
	private String stateName;
	
	public SwitchState(String stateName) {
		this.stateName = stateName;
	}

	@Override
	public void doAction(Lexer lexer) {
		lexer.setLexerState(stateName);
	}

}
