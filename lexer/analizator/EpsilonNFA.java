package analizator;
import java.io.Serializable; 
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * An Epsilon Non-Deterministic Finite Automata. 
 * 
 * This automata is able to transition from a single state to one or many states with a single character.
 * The input to this automata is acceptable if after going through all of the transitions the automata contains at least one
 * acceptable state in it's transition list.
 *
 *
 */
public class EpsilonNFA implements Serializable {

	private static final long serialVersionUID = 8592961458258830292L;
	
	/** Number of unique automata states. */
	int noOfStates;
	
	/** List of all of the automata states. The first state is the beginning state and the last state is the acceptable state.*/
	List<ENFAState> states;
	
	/** Current states in which the Epsilon NFA is in.*/
	Set<ENFAState> transitionalStates;
	
	/** Set which contains the states that were transitioned to from the transitional states with a given character.*/
	Set<ENFAState> newStates;
	
	/** The acceptable NFA state.*/
	ENFAState acceptable;
	
	/** The regex from which this {@link EpsilonNFA} was created.*/
	String regex;

	/** Default constructor. Initializes EpsilonNFA.*/
	public EpsilonNFA() {
		states = new ArrayList<ENFAState>();
		transitionalStates = new HashSet<ENFAState>();
		newStates = new HashSet<ENFAState>();
	}
	
	/** Initializes EpsilonNFA and binds a regex to it.*/
	public EpsilonNFA(String regex) {
		states = new ArrayList<ENFAState>();
		transitionalStates = new HashSet<ENFAState>();
		newStates = new HashSet<ENFAState>();
		this.regex = regex;
	}

	/**
	 * Defines a new transition for the automata.
	 * @param stateA The starting state.
	 * @param stateB The state to which the Epsilon NFA transitions too with the given character.
	 * @param transitionChar Transitional character
	 * @return <code>true</code> if a new state was added to the Epsilon NFA, <code>false</code> if the transition is already defined for the Epsilon NFA.
	 */
	public boolean addTransition(ENFAState stateA, ENFAState stateB, Character transitionChar) {
		return states.get(stateA.getStateNo()).addTransition(transitionChar, states.get(stateB.getStateNo()));
	}

	/**
	 * Defines an epsilon transition for the automata.
	 * @param stateA The starting state.
	 * @param stateB The state to which the Epsilon NFA transitions too.
	 * @return <code>true</code> if a new state was added to the Epsilon NFA, <code>false</code> if the transition is already defined for the Epsilon NFA.
	 */
	public boolean addEpsilonTransition(ENFAState stateA, ENFAState stateB) {
		return stateA.addEpsilonTransition(stateB);
	}
	
	/**
	 * Initializes Epsilon NFA starting conditions.
	 * The starting set of states will contain the begin state and it's epsilon surrounding.
	 */
	public void initialize() {
		transitionalStates.clear();
		transitionalStates.add(states.get(0));
		calculateEpsilonSurrounding();
	}
	
	/**
	 * Calculates the next state set for the given character.
	 * @param transitionChar Transitional character.
	 */
	public boolean calculateTransition(Character transitionChar) {
		calculateCharTransition(transitionChar);
		calculateEpsilonSurrounding();
		return !transitionalStates.isEmpty();
	}
	
	/**
	 * Returns <code>true</code> if the set of transitional states contains an acceptable state and <code>false</code> otherwise.
	 */
	public boolean isAcceptable() {
		return transitionalStates.contains(acceptable);
	}
	
	/**
	 * Calculates the epsilon surrounding of the current transitional states.
	 */
	private void calculateEpsilonSurrounding() {
		int oldSize = transitionalStates.size();
		int newSize = oldSize;
		do {
			oldSize = newSize;
			newStates.clear();
			for(ENFAState state : transitionalStates) {
				List<ENFAState> states = state.getEpsilonTransitions();
				if(states != null) {
					newStates.addAll(states);
				}
			}
			transitionalStates.addAll(newStates);
			newSize = transitionalStates.size();
		} while(oldSize != newSize);
	}
	
	/**
	 * Calculates transition for a single character, but doesn't calculate the epsilon transition.
	 */
	private void calculateCharTransition(Character transitionChar) {
		newStates.clear();
		for(ENFAState state : transitionalStates) {
			List<ENFAState> transitions = state.getTransitions(transitionChar);
			if(transitions != null) {
				newStates.addAll(state.getTransitions(transitionChar));
			}
		}
		transitionalStates.clear();
		transitionalStates.addAll(newStates);
	}
	
	/**
	 * Adds new state to the state list.
	 */
	public ENFAState newState() {
		ENFAState newState = new ENFAState(noOfStates++);
		states.add(newState);
		return newState;
	}

	@Override
	public String toString() {
		return "REGEX: " +  regex;
	}
	

	public String getRegex() {
		return regex;
	}

	public void setRegex(String regex) {
		this.regex = regex;
	}
	
	public Set<ENFAState> getTransitionalStates() {
		return transitionalStates;
	}
	
	public ENFAState getAcceptable() {
		return acceptable;
	}

	public void setAcceptable(ENFAState acceptable) {
		this.acceptable = acceptable;
	}
	
}
