package analizator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Lexer {
	/** Character array of the whole text being lexed.*/
	private char[] lexText;
	/** Keeps track of the line the lexer is currently lexing through.*/
	private int lineCount;
	/** The current lexer state. The lexer state defines which {@link LexerRule}s the lexer uses.*/
	private String lexerState;
	/** A map, the keys of which are lexer state names and the values are {@link LexerRule}s.*/
	private HashMap<String, List<LexerRule>> rules;
	/** The current set of {@link LexerRule}s that the lexer can effectively see.*/
	private List<LexerRule> currentRules;
	/** The starting position from which the lexer is lexing the next lexical unit.*/
	private int beginIndex;
	/** The position of the character the lexer is currently lexing.*/
	private int currentIndex;
	/** Currently grouped text.*/
	private String groupedText;
	/**The currently accepted rule.*/
	private LexerRule currentRule;
	/**Counts how many characters the currently largest recognized lexical unit has.*/
	private int longestTextCounter;
	
	/**
	 * Lexer constructor.
	 * @param lexText - The input text to be lexed
	 * @param lexerState - The beginning lexer state
	 * @param rules - Lexer rules
	 */
	public Lexer(char[] lexText, String lexerState, HashMap<String, List<LexerRule>> rules) {
		this.lexText = lexText;
		this.lineCount = 1;
		this.lexerState = lexerState;
		this.rules = rules;
		this.currentRules = new ArrayList<LexerRule>(rules.get(lexerState));
		this.beginIndex = 0;
		this.currentIndex = 0;
		this.longestTextCounter = 0;
	}
	
	/**
	 * Lexes a single character. <br>
	 * If there is a currently acceptable rule and there are no more transitionable Epsilon NFAs, the lexer will accept the lexical unit.
	 * If there is no currently acceptable rule and there are no transitionable Epsilon NFAs, the lexer will try to recover from the mistake.
	 */
	public void lex() {
		if(lexText.length != currentIndex) {
			LexerRule tempRule = null;
			List<LexerRule> removedRules = new ArrayList<LexerRule>();
			for(LexerRule rule : currentRules) {
				char currentChar = lexText[currentIndex];
				if(rule.getNfa().calculateTransition(currentChar)) {
					if(rule.getNfa().isAcceptable()) {
						longestTextCounter = currentIndex -  beginIndex + 1;
						if(tempRule == null) {
							tempRule = rule;
						}
					}
				} else {
					removedRules.add(rule);
				}
			}
			currentRules.removeAll(removedRules);
			if(currentRule != tempRule && tempRule != null) {
				currentRule = tempRule;
			}
		}
		if(currentRules.isEmpty() || (lexText.length) == currentIndex) {
			if(currentRule == null) {
				recoverFromMistake();
				return;
			} else {
				acceptLexUnit();
				return;
			}
		}
		currentIndex++;
	}
	/**
	 * Accepts lexical unit. <br>
	 * This means that all of the actions defined by the {@link #currentRule} will be executed.
	 */
	private void acceptLexUnit() {
		groupText(longestTextCounter);
		currentRule.doActions(this);
		currentRule = null;
		beginIndex = currentIndex;
		longestTextCounter = 0;
		this.currentRules = new ArrayList<LexerRule>(rules.get(lexerState));
		initializeRules();
	}
	
	/**
	 * Recovers from mistake. <br>
	 * This means that the lexer will write the line number and the longest lexed unit of text that was lexed before mistake recovery.
	 */
	public void recoverFromMistake() {
		groupText(currentIndex -  beginIndex + 1); 
		beginIndex++;
		currentIndex = beginIndex;
		longestTextCounter = 0;
		this.currentRules = new ArrayList<LexerRule>(rules.get(lexerState));
		initializeRules();
	}

	public void incrementLine() {
		lineCount++;
	}
	
	public int getLineCount() {
		return lineCount;
	}

	public String getLexerState() {
		return lexerState;
	}
	
	public void setCurrentIndex(int currentIndex) {
		this.currentIndex = currentIndex;
	}

	private void initializeRules() {
		for(LexerRule rule : currentRules) {
			rule.getNfa().initialize();
		}
	}

	public void setLexerState(String lexerState) {
		this.lexerState = lexerState;
		this.currentRules = new ArrayList<LexerRule>(rules.get(lexerState));;
	}

	public int getBeginIndex() {
		return beginIndex;
	}

	public void setBeginIndex(int beginIndex) {
		this.beginIndex = beginIndex;
	}

	/**
	 * Writes the line and the text of the recognized lexical unit.
	 */
	public void processLexicalUnit(LexicalUnit lexUnit) {
		lexUnit.setLinePosition(lineCount);
		lexUnit.setGroupedText(groupedText);
		System.out.println(lexUnit);
	}

	/**
	 * Groups text from the input text.
	 * @param relativePosition - Number of characters to be grouped relative to the {@link #currentIndex}.
	 */
	public void groupText(int relativePosition) {
		StringBuilder builder = new StringBuilder();
		for(int i = beginIndex; i < beginIndex + relativePosition; i++) {
			builder.append(lexText[i]);
		}
		groupedText = builder.toString();
	}
	
	/**
	 * Will it ever be done?
	 */
	public boolean isDone() {
		return currentIndex == lexText.length;
	}
	
}
