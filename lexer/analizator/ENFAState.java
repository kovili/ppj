package analizator;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;
/**
 * Class representing a single state in an Epsilon NFA.
 *
 *
 */
public class ENFAState implements Serializable{
	
	private static final long serialVersionUID = -4306447018153911437L;

	/**
	 * Mapping of all the transitions from this state to the next.
	 * 
	 * The key is the transition character and the value is the list of {@link ENFAState}s to which the Epsilon NFA transitions
	 * to from this state.
	 */
	private HashMap<Character, List<ENFAState>> transitions;
	
	/** Unique state identifier.*/
	private int stateNo;
	
	/** List of all epsilon transitions for this {@link ENFAState}.*/
	private List<ENFAState> epsilonTransitions;
	
	/**
	 * Initializes hash map.
	 * @param stateNo Unique identifier
	 */
	public ENFAState(int stateNo) {
		this.stateNo = stateNo;
		transitions = new HashMap<Character, List<ENFAState>>();
		epsilonTransitions = new ArrayList<ENFAState>();
	}
	
	/**
	 * Retrieves the list of {@link ENFAState}s to which the corresponding Epsilon NFA transitions to with the given character.
	 * @param input Transitional character
	 * @return {@link List} of states in which the Epsilon NFA is in after the transition.
	 */
	public List<ENFAState> getTransitions(Character input) {
		return transitions.get(input);
	}
	
	public List<ENFAState> getEpsilonTransitions() {
		return epsilonTransitions;
	}
	
	public boolean addEpsilonTransition(ENFAState transitionState) {
		if(!epsilonTransitions.contains(transitionState)) {
			return epsilonTransitions.add(transitionState);
		}
		return false;
	}
	
	/**
	 * Defines a new transition for this state.
	 * @param input Transitional character
	 * @param transitionState The state to which the Epsilon NFA transitions too with the given character.
	 * @return <code>true</code> if a new state was added to the Epsilon NFA, <code>false</code> if the transition is already defined for the Epsilon NFA.
	 */
	public boolean addTransition(Character input, ENFAState transitionState) {
		if(!transitions.containsKey(input)) {
			transitions.put(input, new ArrayList<ENFAState>());
		}
		List<ENFAState> oldTransitions = transitions.get(input);
		if(!oldTransitions.contains(transitionState)) {
			return oldTransitions.add(transitionState);
		}
		return false;
	}
	
	/**
	 * Gets state unique identifier.
	 */
	public int getStateNo() {
		return stateNo;
	}

	@Override
	public int hashCode() {
		return Objects.hash(stateNo);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof ENFAState))
			return false;
		ENFAState other = (ENFAState) obj;
		return stateNo == other.stateNo;
	}
	
	
}
