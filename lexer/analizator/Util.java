package analizator;
import java.util.HashMap; 
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Util class with helper methods.
 */
public class Util {
	
	/**
	 * Finds the regex in a line and puts it in the given regex map.
	 * @param line -  Line with regex name and regex defintition
	 * @param regDefs - Regex map
	 */
	public static void extractRegularDefinition(String line, HashMap<String, String> regDefs) {
		String []definition = line.split(" ");
		String name = definition[0];
		
		String regex = definition[1].replaceAll("\\\\_", " ");
		
		regex = resolveRegex(regex, regDefs);
		regDefs.put(name, regex);
	}


	/**
	 * Resolves escaping and references in regexes.
	 * @return Resolved regex
	 */
	public static String resolveRegex(String regex, HashMap<String, String> regDefs) {
		while(regex.matches(".*\\{.+\\}.*")) {
			String reference = captureReference(regex);
			String replacement = regDefs.get(reference);
			if(replacement != null) {
				Pattern pattern = Pattern.compile("\\{[^\\}]+\\}");
				Matcher matcher = pattern.matcher(regex);
				regex = matcher.replaceFirst("(" + Matcher.quoteReplacement(replacement) + ")");
			} else {
				break;
			}
		}
		return regex;
	}

	/**
	 * Finds regex reference name.
	 * @return Regex reference name
	 */
	private static String captureReference(String regex) {
		char []characters = regex.toCharArray();
		StringBuilder builder = new StringBuilder();
		boolean openBracket = false;
		for(int i = 0; i < characters.length; i++) {
			if((characters[i] == '{' && isOperator(characters, i)) || openBracket == true) {
				openBracket = true;
				builder.append(characters[i]);
			}
			if(characters[i] == '}' && isOperator(characters, i)) {
				break;
			}
		}
		return builder.toString();
	}
	
	/**Checks if the given character with the given given index in the given list is an operator given given given the escaping.*/
	private static boolean isOperator(char []expression, int index) {
		int counter = 0;
		while(index - 1 >= 0 && expression[index - 1] == ('\\')) {
			counter++;
			index--;
		}
		return counter % 2 == 0;
	}
	
	/**Finds all state names and puts them in the given list.*/
	public static void extractStates(List<String> lexerStates, String stateDefinition) {
		String []states = stateDefinition.split(" ");
		for(int i = 1; i < states.length; i++) {
			lexerStates.add(states[i]);
		}
	}
	
	/**Finds all lexical unit names and puts them in the given list.*/
	public static void extractLexicalUnits(List<String> lexicalUnits, String lexicalUnitsDefinition) {
		String []states = lexicalUnitsDefinition.split(" ");
		for(int i = 1; i < states.length; i++) {
			lexicalUnits.add(states[i]);
		}
	}
	
	public static String extractStateName(char[] letters) {
		StringBuilder builder = new StringBuilder();
		boolean readingState = false;
		for(char letter : letters) {
			if(readingState == true && letter != '>') {
				builder.append(letter);
			} else if(letter == '<') {
				readingState = true;
			} else if(letter == '>') {
				break;
			}
		}
		return builder.toString();
	}
	
	public static String extractRegex(char[] letters) {
		StringBuilder builder = new StringBuilder();
		boolean readingRegex = false;
		for(char letter : letters) {
			if(readingRegex == true) {
				builder.append(letter);
			}else if(letter == '>') {
				readingRegex = true;
			}
		}
		return builder.toString().replaceAll("\\\\_", " ");
	}
	

}
