package analizator;
/**
 * A simple container for two states.
 */
public class StatePair {
	/**The left state DUH.*/
	public ENFAState leftState;
	/**The right state DUH.*/
	public ENFAState rightState;
	
	public StatePair(ENFAState leftState, ENFAState rightState) {
		this.leftState = leftState;
		this.rightState = rightState;
	}
}
