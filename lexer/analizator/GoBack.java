package analizator;

/**
 * Sets the Lexer current position to (currentPosition - relativePosition).
 *
 */
public class GoBack implements Action {
	private static final long serialVersionUID = -545454070681213471L;
	/**Defines by how many characters should the lexer go back.*/
	private int relativePosition;

	public GoBack(int relativePosition) {
		this.relativePosition = relativePosition;
	}
	
	@Override
	public void doAction(Lexer lexer) {
		lexer.groupText(relativePosition);
		lexer.setBeginIndex(lexer.getBeginIndex() + relativePosition);
		lexer.setCurrentIndex(lexer.getBeginIndex());
	}

}
