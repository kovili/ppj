package analizator;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * A rule for a lexer which contains actions the lexer must do if the rule is accepted.
 */
public class LexerRule implements Serializable{
	private static final long serialVersionUID = -1099592521849877921L;
	/**If this nfa accepts a string, this rule will become acceptable (executable).*/
	private EpsilonNFA nfa;
	/**The actions the lexer must do when this rule is accepted.*/
	private List<Action> actions;
	
	public LexerRule(EpsilonNFA nfa) {
		this.nfa = nfa;
		actions = new ArrayList<Action>();
	}
	
	public EpsilonNFA getNfa() {
		return nfa;
	}

	public boolean addAction(Action action) {
		return actions.add(action);
	}

	/**Executes actions*/
	public void doActions(Lexer lexer) {
		for(Action action : actions) {
			action.doAction(lexer);
		}
	}

	@Override
	public String toString() {
		return "REGEX: " + nfa.getRegex();
	}
	
	
	
	
}
