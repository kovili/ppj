package analizator;
import java.util.ArrayList;
import java.util.List;

/**
 * Generates Epsilon NFA from the given regex.
 *
 */
public class EpsilonNFAGenerator {
	
	/**
	 * Generates Epsilon NFA from the given regex.
	 * @param regex -  Regular expression
	 * @return An Epsilon NFA which accepts if and only if a string matches the regex from which this NFA was created
	 */
	public static EpsilonNFA generateEpsilonNFA(String regex) {
		EpsilonNFA nfa = new EpsilonNFA(regex);
		List<Character> regexChars = new ArrayList<Character>();
		char []characters = regex.toCharArray();
		for(char character : characters) {
			regexChars.add(character);
		}
		StatePair pair = buildAutomata(nfa, regexChars);
		nfa.setAcceptable(pair.rightState);
		return nfa;
	}
	
	/**
	 * Recursive algorithm for creating the Epsilon NFA.
	 * @param nfa
	 * @param regexChars
	 * @return
	 */
	private static StatePair buildAutomata(EpsilonNFA nfa, List<Character> regexChars) {
		List<List<Character>> choices = new ArrayList<List<Character>>();
		int bracketCounter = 0;
		int beginSublistIndex = 0;
		
		for(int i = 0; i < regexChars.size(); i++) {
			if(regexChars.get(i).equals('(') && isOperator(regexChars, i)) {
				bracketCounter++;
			}else if(regexChars.get(i).equals(')') && isOperator(regexChars, i)) {
				bracketCounter--;
			} else if (bracketCounter == 0 && regexChars.get(i) == '|' && isOperator(regexChars, i)) {
				choices.add(regexChars.subList(beginSublistIndex, i));
				beginSublistIndex = i + 1;
			}
		}
		
		choices.add(regexChars.subList(beginSublistIndex, regexChars.size()));
		
		ENFAState leftState = nfa.newState();
		ENFAState rightState = nfa.newState();
		
		if(choices.size() > 1) {
			for(int i = 0; i < choices.size(); i++) {
				StatePair temporaryPair = buildAutomata(nfa, choices.get(i));
				nfa.addEpsilonTransition(leftState, temporaryPair.leftState);
				nfa.addEpsilonTransition(temporaryPair.rightState, rightState);
			}
		} else {
			boolean hasPrefix = false;
			ENFAState lastState = leftState;
			
			for(int i = 0; i < regexChars.size(); i++) {
				ENFAState a;
				ENFAState b;
				
				if(hasPrefix) {
					hasPrefix = false;
					Character transitionalCharacter;
					if(regexChars.get(i).equals('t')) {
						transitionalCharacter = '\t';
					} else if(regexChars.get(i).equals('n')) {
						transitionalCharacter = '\n';
					} else if(regexChars.get(i).equals('_')) {
						transitionalCharacter = ' ';
					} else {
						transitionalCharacter = regexChars.get(i);
					}
					
					a = nfa.newState();
					b = nfa.newState();
					nfa.addTransition(a, b, transitionalCharacter);
				} else {
					if(regexChars.get(i).equals('\\')) {
						hasPrefix = true;
						continue;
					} 
					if(!regexChars.get(i).equals('(')) {
						a = nfa.newState();
						b = nfa.newState();
						if(regexChars.get(i).equals('$')) {
							nfa.addEpsilonTransition(a, b);
						} else {
							nfa.addTransition(a, b, regexChars.get(i));
						}
					} else {
						int j = findCloseBracket(regexChars, i);
						StatePair temporaryPair = buildAutomata(nfa, regexChars.subList(i + 1, j)); 
						a = temporaryPair.leftState;
						b = temporaryPair.rightState;
						i = j;
					}
				}
				
				if(i + 1 < regexChars.size() && regexChars.get(i + 1) == '*') {
					ENFAState x = a;
					ENFAState y = b;
					a = nfa.newState();
					b = nfa.newState();
					nfa.addEpsilonTransition(a, x);
					nfa.addEpsilonTransition(y, b);
					nfa.addEpsilonTransition(a, b);
					nfa.addEpsilonTransition(y, x);
					i++;
				}
				
				nfa.addEpsilonTransition(lastState, a);
				lastState = b;
			}
			nfa.addEpsilonTransition(lastState, rightState);
		}
		
		return new StatePair(leftState, rightState);
	}

	private static int findCloseBracket(List<Character> regexChars, int index) {
		int bracketCounter = 0;
		for(int i = index; index < regexChars.size(); i++) {
			if(regexChars.get(i).equals('(') && isOperator(regexChars, i)) {
				bracketCounter++;
			}else if(regexChars.get(i).equals(')') && isOperator(regexChars, i)) {
				bracketCounter--;
			}
			
			if(bracketCounter == 0) {
				return i;
			}
		}
		return -1;
	}

	/**Checks if the given character with the given given index in the given list is an operator given given given the escaping.*/
	private static boolean isOperator(List<Character> expression, int index) {
		int bracketCounter = 0;
		while(index - 1 >= 0 && expression.get(index - 1).equals('\\')) {
			bracketCounter++;
			index--;
		}
		return bracketCounter % 2 == 0;
	}
}
