package analizator;
import java.io.Serializable; 
/**
 * An action the {@link Lexer} can preform.
 *
 */
public interface Action extends Serializable {
	
	/**
	 * Executes action.
	 * @param lexer -  The lexer on which the action is executed
	 */
	void doAction(Lexer lexer);
}
