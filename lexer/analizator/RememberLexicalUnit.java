package analizator;

/**
 * Remembers a lexical unit and gives it to the lexer for processing when executing the action.
 */
public class RememberLexicalUnit implements Action{
	private static final long serialVersionUID = -9201405641721074713L;
	/**The soon to be processed lexical unit.*/
	LexicalUnit lexUnit;
	
	public RememberLexicalUnit(String uniformSign) {
		this.lexUnit = new LexicalUnit(uniformSign);
	}

	@Override
	public void doAction(Lexer lexer) {
		lexer.processLexicalUnit(lexUnit);
	}

}
