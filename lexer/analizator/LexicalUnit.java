package analizator;

import java.io.Serializable;

/**
 * A single lexical unit.
 */
public class LexicalUnit implements Serializable {
	private static final long serialVersionUID = 8190979074117053281L;
	/**Line position in which the unit was found.*/
	private int linePosition;
	/**The name of the lexical unit.*/
	private String uniformSign;
	/**Captured text.*/
	private String groupedText;
	
	public LexicalUnit(int linePosition, String uniformSign, String groupedText) {
		this.linePosition = linePosition;
		this.uniformSign = uniformSign;
		this.groupedText = groupedText;
	}

	public LexicalUnit(String uniformSign) {
		super();
		this.uniformSign = uniformSign;
	}

	public int getLinePosition() {
		return linePosition;
	}

	public String getUniformSign() {
		return uniformSign;
	}

	public String getGroupedText() {
		return groupedText;
	}

	public void setLinePosition(int linePosition) {
		this.linePosition = linePosition;
	}

	public void setGroupedText(String groupedText) {
		this.groupedText = groupedText;
	}

	@Override
	public String toString() {
		return uniformSign + " " + linePosition + " " + groupedText;
	}
	
	
	
}
