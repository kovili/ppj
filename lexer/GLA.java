import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectOutputStream;
import java.io.OutputStreamWriter;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import analizator.Action;
import analizator.EpsilonNFAGenerator;
import analizator.EpsilonNFA;
import analizator.GoBack;
import analizator.LexerRule;
import analizator.NewLine;
import analizator.RememberLexicalUnit;
import analizator.SwitchState;
import analizator.Util;
/**
 * Lexer generator.<br>
 * Parses input file for lexer definition defined by:
 * 	<ul>
 * 		<li><code>Regular definition</code> regular definition</li>
 * 		<li><code>States</code> states state_name1 state_name2 ... stateNameN</li>
 * 		<li><code>Lexical units</code> lex_unit_name1 lex_unit_name2 ... lex_unit_nameN</li>
 * 		<li><code>Lexer rules</code>
 * </ul>
 * 
 * After extracting lexer information, the lexer generator:
 * <ol>
 * 		<li>Creates Epsilon NKAs for the given regular expressions</li>
 * 		<li>Creates a map (<b>Rule map</b>) which contains all of the {@link LexerRule}s for every state</li>
 * 		<li>Writes the <b>Rule map</b> into a file using an {@link ObjectOutputStream}</li>
 * 		<li>Writes the lexer analizer program into a file</li>
 * </ol>
 */
public class GLA {
	public static void main(String[] args) throws IOException {
		long start = System.currentTimeMillis();
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		String line;
		HashMap<String, String> regDefs = new HashMap<String, String>();
		List<String> lexerStates = new ArrayList<String>();
		List<String> lexicalUnits = new ArrayList<String>();
		HashMap<String, List<LexerRule>> stateRules = new HashMap<String, List<LexerRule>>();
		String startState = null;
		
		while(reader.ready()) {
			line = reader.readLine();
			//Regular definitions
			if(line.startsWith("{")) {
				Util.extractRegularDefinition(line, regDefs);
			//Lexer states
			} else if(line.startsWith("%X")) {
				Util.extractStates(lexerStates, line);
				for(String lexerState : lexerStates) {
					stateRules.put(lexerState, new ArrayList<LexerRule>());
				}
			//Uniform Signs
			} else if(line.startsWith("%L")) {
				Util.extractLexicalUnits(lexicalUnits, line);
			//Lexer rules
			} else if(line.startsWith("<")) {
				char []letters = line.toCharArray();
				String stateName = Util.extractStateName(letters);
				if(startState == null) {
					startState = stateName;
				}
				EpsilonNFA nfa = 
						EpsilonNFAGenerator.generateEpsilonNFA(Util.resolveRegex(Util.extractRegex(letters), regDefs));
				nfa.initialize();
				LexerRule lexerRule = new LexerRule(nfa);
				
				//Open curly bracket {
				reader.readLine();
				
				//First line which contains uniform sign or - (none)
				String uniformSign = reader.readLine();
				Action rememberLexUnit = null;
				if(!uniformSign.equals("-")) {
					rememberLexUnit = new RememberLexicalUnit(uniformSign);
				}
				
				//Read arguments
				while(reader.ready()) {
					line = reader.readLine();
					if(line.startsWith("}")) {
						break;
					}
					if(line.startsWith("UDJI_U_STANJE")) {
						lexerRule.addAction(new SwitchState(line.split(" ")[1]));
					} else if(line.startsWith("VRATI_SE")) {
						lexerRule.addAction(new GoBack(Integer.parseInt(line.split(" ")[1])));
					} else if(line.startsWith("NOVI_REDAK")) {
						lexerRule.addAction(new NewLine());
					}
				}
				if(rememberLexUnit != null) {
					lexerRule.addAction(rememberLexUnit);
				}
				
				//Add rule to state
				stateRules.get(stateName).add(lexerRule);
			}
		}
		reader.close();
		System.err.println(Paths.get("").toAbsolutePath().toString());
		FileOutputStream fos = 
				new FileOutputStream(Paths.get("").toAbsolutePath().toString() + "/src/analizator/rules.ser");
		ObjectOutputStream oos = new ObjectOutputStream(fos);
		oos.writeObject(stateRules);
		oos.close();
		fos.close();
		String lexer = "package analizator;\r\n" + 
				"import java.io.BufferedReader;\r\n" + 
				"import java.io.FileInputStream;\r\n" + 
				"import java.io.IOException;\r\n" + 
				"import java.io.InputStreamReader;\r\n" + 
				"import java.io.ObjectInputStream;\r\n" + 
				"import java.nio.file.Paths;\r\n" + 
				"import java.util.HashMap;\r\n" + 
				"import java.util.List;\r\n" + 
				"public class LA {\r\n" + 
				"	public static void main(String[] args) throws IOException, ClassNotFoundException {\r\n" + 
				"		long start = System.currentTimeMillis();\r\n" + 
				"		BufferedReader buff = new BufferedReader(new InputStreamReader(System.in));\r\n" + 
				"		StringBuilder builder = new StringBuilder();\r\n" + 
				"		while(buff.ready()) {\r\n" + 
				"			builder.append((char) buff.read());\r\n" + 
				"		}\r\n" + 
				"		buff.close(); \r\n" +
				"		FileInputStream fis = new FileInputStream(Paths.get(\"\").toAbsolutePath().toString() + \"/src/analizator/rules.ser\");\r\n" + 
				"		ObjectInputStream ois = new ObjectInputStream(fis);\r\n" + 
				"		HashMap<String, List<LexerRule>> rules = (HashMap<String, List<LexerRule>>) ois.readObject();\r\n" + 
				"		ois.close();\r\n" +
				"		Lexer lexer = new Lexer(builder.toString().toCharArray(), \"" + lexerStates.get(0) +  "\", rules);\r\n" + 
				"		while(!lexer.isDone()) {\r\n" + 
				"			lexer.lex();\r\n" + 
				"		}\r\n" + 
				"		System.err.println(\"Program took \" +  (System.currentTimeMillis() - start) +  \"ms to execute.\");\r\n" +
				"	}\r\n" + 
				"	\r\n" + 
				"}";
		BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(Paths.get("").toAbsolutePath().toString() + "/src/analizator/LA.java")));
		writer.write(lexer);
		writer.flush();
		writer.close();
		System.err.println("Program took " +  (System.currentTimeMillis() - start) +  "ms to execute.");
	}
	
}
