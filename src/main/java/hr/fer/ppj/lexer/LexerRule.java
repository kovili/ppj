package hr.fer.ppj.lexer;

import java.util.ArrayList;
import java.util.List;

import hr.fer.ppj.actions.Action;
import hr.fer.ppj.nfa.Epsilon_NFA;

public class LexerRule {
	private Epsilon_NFA nfa;
	private List<Action> actions;
	
	public LexerRule(Epsilon_NFA nfa) {
		this.nfa = nfa;
		actions = new ArrayList<Action>();
	}
	
	public boolean addAction(Action action) {
		return actions.add(action);
	}
	
	
}
