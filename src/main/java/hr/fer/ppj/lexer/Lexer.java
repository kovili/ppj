package hr.fer.ppj.lexer;

import java.util.HashMap;
import java.util.List;

public class Lexer {
	
	/**
	 * Character array of the whole text being lexed.
	 */
	private char[] lexText;
	/**
	 * Keeps track of the line the lexer is currently lexing through.
	 */
	private int lineCount;
	/**
	 * The current lexer state. The lexer state defines which {@link LexerRule}s the lexer uses.
	 */
	private String lexerState;
	/**
	 * A map, the keys of which are lexer state names and the values are {@link LexerRule}s.
	 */
	private HashMap<String, List<LexerRule>> rules;
	/**
	 * The current set of {@link LexerRule}s that the lexer can effectively see.
	 */
	private List<LexerRule> currentRules;
	/**
	 * The starting position from which the lexer is lexing the next lexical unit.
	 */
	private int beginIndex;
	/**
	 * The position of the character the lexer is currently lexing.
	 */
	private int currentIndex;
	
	/**
	 * Currently grouped text.
	 */
	private String groupedText;
	
	public Lexer() {
		// TODO Auto-generated constructor stub
	}

	public void incrementLine() {
		lineCount++;
	}
	
	public int getLineCount() {
		return lineCount;
	}

	public String getLexerState() {
		return lexerState;
	}

	public void setLexerState(String lexerState) {
		this.lexerState = lexerState;
		currentRules = rules.get(this.lexerState);
	}

	public int getBeginIndex() {
		return beginIndex;
	}

	public void setBeginIndex(int beginIndex) {
		this.beginIndex = beginIndex;
	}

	public void processLexicalUnit(LexicalUnit lexUnit) {
		lexUnit.setLinePosition(lineCount);
		lexUnit.setGroupedText(groupedText);
		System.out.println(lexUnit);
	}

	public void groupText(int relativePosition) {
		StringBuilder builder = new StringBuilder();
		for(int i = beginIndex; i < beginIndex + relativePosition; i++) {
			builder.append(lexText[i]);
		}
		groupedText = builder.toString();
	}
	
}
