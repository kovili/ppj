package hr.fer.ppj.lexer;

public class LexicalUnit {
	private int linePosition;
	private String uniformSign;
	private String groupedText;
	
	public LexicalUnit(int linePosition, String uniformSign, String groupedText) {
		this.linePosition = linePosition;
		this.uniformSign = uniformSign;
		this.groupedText = groupedText;
	}

	public LexicalUnit(String uniformSign) {
		super();
		this.uniformSign = uniformSign;
	}

	public int getLinePosition() {
		return linePosition;
	}

	public String getUniformSign() {
		return uniformSign;
	}

	public String getGroupedText() {
		return groupedText;
	}

	public void setLinePosition(int linePosition) {
		this.linePosition = linePosition;
	}

	public void setGroupedText(String groupedText) {
		this.groupedText = groupedText;
	}

	@Override
	public String toString() {
		return uniformSign + " " + linePosition + " " + groupedText;
	}
	
	
	
}
