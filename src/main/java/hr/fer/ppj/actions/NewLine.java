package hr.fer.ppj.actions;

import hr.fer.ppj.lexer.Lexer;

public class NewLine implements Action {

	@Override
	public void doAction(Lexer lexer) {
		lexer.incrementLine();
	}

}
