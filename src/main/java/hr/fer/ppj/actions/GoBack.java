package hr.fer.ppj.actions;

import hr.fer.ppj.lexer.Lexer;

public class GoBack implements Action {
	private int relativePosition;

	public GoBack(int relativePosition) {
		this.relativePosition = relativePosition;
	}
	
	@Override
	public void doAction(Lexer lexer) {
		lexer.groupText(relativePosition);
		lexer.setBeginIndex(lexer.getBeginIndex() + relativePosition);
	}

}
