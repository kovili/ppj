package hr.fer.ppj.actions;

import hr.fer.ppj.lexer.Lexer;

public class SwitchState implements Action {
	private String stateName;
	
	public SwitchState(String stateName) {
		this.stateName = stateName;
	}

	@Override
	public void doAction(Lexer lexer) {
		lexer.setLexerState(stateName);
	}

}
