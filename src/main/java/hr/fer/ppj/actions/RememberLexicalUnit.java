package hr.fer.ppj.actions;

import hr.fer.ppj.lexer.Lexer;
import hr.fer.ppj.lexer.LexicalUnit;

public class RememberLexicalUnit implements Action{
	LexicalUnit lexUnit;
	
	public RememberLexicalUnit(String uniformSign) {
		this.lexUnit = new LexicalUnit(uniformSign);
	}

	@Override
	public void doAction(Lexer lexer) {
		lexer.processLexicalUnit(lexUnit);
	}

}
