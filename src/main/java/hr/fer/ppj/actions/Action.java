package hr.fer.ppj.actions;

import hr.fer.ppj.lexer.Lexer;

public interface Action {
	
	void doAction(Lexer lexer);
}
