package hr.fer.ppj;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import hr.fer.ppj.actions.Action;
import hr.fer.ppj.actions.GoBack;
import hr.fer.ppj.actions.NewLine;
import hr.fer.ppj.actions.RememberLexicalUnit;
import hr.fer.ppj.actions.SwitchState;
import hr.fer.ppj.lexer.LexerRule;
import hr.fer.ppj.nfa.EpsilonNFAGenerator;
import hr.fer.ppj.nfa.Epsilon_NFA;

public class Input {
	public static void main(String[] args) throws IOException {
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		String line;
		HashMap<String, String> regDefs = new HashMap<String, String>();
		List<String> lexerStates = new ArrayList<String>();
		List<String> lexicalUnits = new ArrayList<String>();
		HashMap<String, List<LexerRule>> stateRules = new HashMap<String, List<LexerRule>>();
		
		while(reader.ready()) {
			line = reader.readLine();
			//Regular definitions
			if(line.matches(".*{.+}.*")) {
				Util.extractRegularDefinition(line, regDefs);
			//Lexer states
			} else if(line.startsWith("%X")) {
				Util.extractStates(lexerStates, line);
				for(String lexerState : lexerStates) {
					stateRules.put(lexerState, new ArrayList<LexerRule>());
				}
			//Uniform Signs
			} else if(line.startsWith("%L")) {
				Util.extractLexicalUnits(lexicalUnits, line);
			//Lexer rules
			} else if(line.startsWith("<")) {
				char []letters = line.toCharArray();
				String stateName = Util.extractStateName(letters);
				Epsilon_NFA nfa = 
						EpsilonNFAGenerator.generateEpsilonNFA(Util.resolveRegex(Util.extractRegex(letters), regDefs));
				LexerRule lexerRule = new LexerRule(nfa);
				
				//Open curly bracket {
				reader.readLine();
				
				//First line which contains uniform sign or - (none)
				String uniformSign = reader.readLine();
				Action rememberLexUnit = null;
				if(!uniformSign.equals("-")) {
					rememberLexUnit = new RememberLexicalUnit(uniformSign);
				}
				
				//Read arguments
				while(reader.ready()) {
					line = reader.readLine();
					if(line.startsWith("}")) {
						break;
					}
					if(line.startsWith("UDJI_U_STANJE")) {
						lexerRule.addAction(new SwitchState(line.split(" ")[1]));
					} else if(line.startsWith("VRATI_SE")) {
						lexerRule.addAction(new GoBack(Integer.parseInt(line.split(" ")[1])));
					} else if(line.startsWith("NOVI_REDAK")) {
						lexerRule.addAction(new NewLine());
					}
				}
				if(rememberLexUnit != null) {
					lexerRule.addAction(rememberLexUnit);
				}
				
				//Add rule to state
				stateRules.get(stateName).add(lexerRule);
			}
		}
		reader.close();
		
	}
	
}
