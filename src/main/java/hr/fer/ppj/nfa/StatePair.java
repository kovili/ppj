package hr.fer.ppj.nfa;

public class StatePair {
	public ENFAState leftState;
	public ENFAState rightState;
	
	public StatePair(ENFAState leftState, ENFAState rightState) {
		this.leftState = leftState;
		this.rightState = rightState;
	}
}
